.. =============================================
.. Intro do vyvoja na ARM (a ako si najst dosku)
.. =============================================
..
..
.. Vďaka nízkej cene a spotrebe energie architektúra ARM stala základom pre mnoho
.. experimentálnych projektov, a pomaly sa presadzuje aj v "mainstream" HPC
.. a serveroch. Na trhu je ale nepreberné množstvo lacných dosiek rôznej kvality
.. a ARM jadrá sa niekedy správajú inak než očakáva vývojár zvyknutý na x86 PC.
..
.. Táto prednáška pomôže vytvoriť si prehľad o tom ako nájsť ideálnu (alebo aspoň
.. použiteľnú) ARM dosku a tiež predstaví odlišnosti v správaní ARM jadier oproti
.. x86 čo sa týka výkonu.



==================
Trains are awesome
==================

.. container:: fullscreen-image-v

   .. image:: train_1.jpg
     :align: center


------------------
Trains are awesome
------------------

.. container:: fullscreen-image-v

   .. image:: train_2.jpg
     :align: center

--------------------------
mmcblk0... probably an ARM
--------------------------

.. container:: fullscreen-image-h

   .. image:: train_3.jpg
     :align: center

--------------------------
mmcblk0... probably an ARM
--------------------------

.. container:: fullscreen-image-h

   .. image:: train_4.png
     :align: center

--------
Ad no. 1
--------

.. container:: hugecentered

   V01D

* Hackerspace in KE ( hackerspaces.org )
* Meetups every other tuesday 19:00:

  Space travel, CPU architectures, optimization, Neo/Vim, hardware hacking,
  window managers, security, literature, gamedev...

  Nearest this ``Tuesday 25.10 19:00``
* Less dead than ever before

* http://v01d.sk
* ``Národná Trieda 74`` (knock on the window with Snowden)
* IRC: ``#void`` @ ``irc.freenode.net``
* Mailing list: ``v01d-general-discussion@lists.v01d.sk``

**No 'enterprise' allowed**

=====================
Choosing an ARM board
=====================

NOTE 1: I'm assuming you're using a solid, non-'enterprise' OSS system.

NOTE 2: This is not a thoroughly researched talk. Pull requests welcome.

Focusing on high-performance.

https://gitlab.com/kiith/cpp-benchmarks

https://gitlab.com/kiith/slides

-------------------
Get a Raspberry Pi.
-------------------

* Unless you *really really* need something specific
* Unless you need to truly be free as in Stallman

--------------
Kernel support
--------------

Often *one* manufacturer-hacked kernel. Forget updates.

So you might get modern Debian... with kernel 3.10.

Good luck:

* Trying to hack a newer kernel to work
* Reconfiguring the kernel (why the **F** did they pick these flags?)
* Kernel-dependent tools/libs (perf, netfilter_queue ...)
* Security

*Make sure available kernel supports what you need*

Or **get a Raspberry Pi**: apparently supported in mainline kernel now

-------
Network
-------

* RasPi and many boards: 100Mbit ethernet
* Can't find board with 2 gigabits?  Find one with gigabit + USB3.

  Watch out for driver support... or slow USB3.

* WiFi?
* Multiple NICs? (e.g. router boards)

  Can the board itself really make use of them?


-------------------------------
Connectivity - specs vs reality
-------------------------------

* Does USB3 really have USB3 speeds?
* What if **multiple** USB ports are used?
* What devices can it **power** without a hub?
* Does SATA/Ethernet/etc go through the USB bus?
* Is gigabit gigabit?

-------------
Upgradability
-------------

* SATA/M.2
* PCIe (x1/x4/x8/x16)?

  useful for more fast NICs; SSDs? even GPUs?

* **DIMM**/**SODIMM** slots ?
* Standard form-factor (Mini-ITX, RasPi-compatible)?
* **SoM** + I/O board combinations

--------------
Other features
--------------

* Codec support?
* GPU cores?
* Virtualization (don't really know about this)?
* 'IoT' protocols?
* GPIO?
* **Cooling** actually required? (e.g. RPi3)

etc.

-------------
ARM ecosystem
-------------

* ARM - CPU designs
* SoC vendors - reference boards, modified CPU designs
* Board vendors

  Get a custom board! $20/piece. Err. $20k/1000pcs
* "Users": Routers, STBs, Internet of Easily Hacked Shit

Lots of competition, fast evolution, PITA standardization


========
ARM CPUs
========

These also kinda matter when picking a board


---------------
Characteristics
---------------

* **Frequency/core count not so important**

* Hardfloat? NEON? - *usually* not a problem anymore (hi Antik)
* In-order/out of order? (instruction parallelism)
* big.LITTLE?
* cache size/parameters?
* ARMv7/v8? Older?


-------------
On big.LITTLE
-------------


* Heterogenous CPU configurations (e.g. 4 big A15 + 4 LITTLE A7)

  Or even 2+4+4

* Save power

.. image:: fire.jpg
   :width: 50%
   :align: right

* Linux may allow you to use **all the coars**

  Not always a good idea

* Some things expect homogenous CPUs

  May not work

  (``perf``)

* LITTLE much less powerful than big

  ``2.0Ghz != 2.0GHz``


---------------
Cortex-A series
---------------

High-end CPUs for '**Application**' use


* Most common 'high-perf' ARM CPU family

* Large differences between cores (think Pentium vs Pentium 3)

* Glossing over:

  **Cortex-R** - Real-time

  **Cortex-M** - Microcontrollers

---------
Cortex-A7
---------

* LITTLE
* Raspberry Pi 2, most BananaPi boards, ODROID XU-4
* In-order CPU
* Low perf regardless of GHz

* Can't emulate Dreamcast
* 8-core A7 not worth much (except embarassingly parallel)

----------
Cortex-A15
----------

* big
* High-end CPU
* Out-of-order
* ODROID XU4, Dragonbox Pyra

* **Can** emulate Dreamcast


---------------
64-bit Cortexes
---------------

* LITTLE: A-53 (Raspberry Pi 3)
* big: A-57, A-72

https://en.wikipedia.org/wiki/Comparison_of_ARMv8-A_cores


----------------
Upcoming - cores
----------------

* **Cortex A73** (30% faster than A72 - nears Intel territory)
* **AMD K12** ('brother' of Zen, similar performance)
* **NVidia Denver 2** (maybe this is what powers Switch?)
* **Cavium Thunder-X** (48 cores) and similar


.. image:: kirin.jpg
   :width: 50%
   :align: right

* Example:

  **Kirin 960**: 4x2.4GHz A73 + 4x1.8GHz A53

  Paired w/ 6GiB LPDDR4-1800 RAM

-----------------------
Upcoming - architecture
-----------------------

* Moore's **observation** no longer holds
* More heterogenous, specialized cores

  - GPU cores sharing memory (APU)
  - Neural network hardware?

* More memory layers

  - HBM
  - X-Point and similar

-------------------
Upcoming - software
-------------------

* More standardization (please?)
* Better mainline kernel support (pretty please?)
* Vulkan and SPIR-V

  More open GPU programming, VR/AR

------------
Where to buy
------------

* ``google.com``
* ``rlx.sk``
* ``aliexpress.com``
* ``rpishop.cz``

etc


====================
ARM core performance
====================

-----------------------
Note on cross-compiling
-----------------------

* Creating a cross-compiling toolchain is a PITA
* Find out if one exists

* Or... **F** cross-compiling, compile on board

  ODROID XU-4 has 8 cores... why use a PC?

* Don't expect copying a binary between boards to work

  (C library, ARMv8 vs v7 vs ..., NEON/hardfloat...)



------------------
Performance vs x86
------------------

.. TODO cache matters! which data structure is better is counterintuitive
   - benchmark! (e.g. hashmap slower than binary search due to memory bloat)
   and benchmark this with varying data (because cache matters)

* Drivers/environment might be crap and slow you down

Slow:

* High-res timer access
* Allocations
* Synchronization
* Non-32bit ops (even on 64bit!)
* Virtual functions if they lead to cache misses
* Cache misses
* Divisions/modulo

**less forgiving to bad code (e.g. Java) than x86**

--------------
benchmarks-cpp
--------------

https://gitlab.com/kiith/cpp-benchmarks

Series of simple C++ benchmarks to get a 'feel' of a CPU

* How fast is addition? Division?
* Are allocations slow?
* How does mutex compare to atomics?

* Results are *best-case* - an operation runs in a loop

* Real-life performance may be worse

**TODO**: basic algorithm benchmarks (varies between CPUs due to e.g.
memory perf)


--------------
benchmarks-cpp
--------------

(demo + observations + arndale notes)

.. Result notes (RPi3):
..
.. * Polymorphism:
..
..   Virtual overhead not so bad. Function call overhead bad in general
.. * Allocations:
..
..   - Malloc is expensive
..   - Big malloc is uber-expensive
..   - Fragmented heap - not so bad
..   - Accessing allocated memory seems no problem w/ small alloc,
..     *huge* problem with large ones - guess the memory is already allocated.
..     This is different from x86, where 16k already sees slowdown
..   - Fragmented heap + access is not much worse on ARM - it is on x86
..   - BTW - stack is really fast. But not for huge arrays. Helps a lot
..     more on ARM due to alloc overhead.
..   - Timing is horrible. Make sure to correct for it when using for
..     profiling. COARSE isn't worth anything. Use standard, it just wraps
..     POSIX and is portable. Per-process/per-thread stuff is even worse...
..     its overhead may cause more deviations than just global timing
..     when the process uses 90%+ of CPU.
..   - Mutices are extremely cheap when not contested.
..     Not all that slow even if contestd by 2 threads.
..   - Atomics surprisingly slow, not sure if this benchmark is fully
..     valid here.
.. * Ops:
..
..   - Float/double same speed except division/fmod
..   - 32bit fastest, everything is a bit slower - including 64bit
..     64bit division/mod is extra slow
..   - Signed-unsigned no difference
..   - Float mult faster than 64bit mult,
..   - Float div faster than int div in general
..   - Throughput better than latency, independent ops can be faster
..     - especially division
..   - Throughput gains lower than on SandyBridge
..   - Exceptions slow, even w/ no unwinding


-----------------
Profiling is hard
-----------------

* ``perf`` may not be work with your snowflake kernel

  but a **huge** win if works - **worth a board just for this**
* ``callgrind`` completely broken on ARM
* ``gprof`` still useless
* manual timing/frame profiling... watch that timing overhead

.. TODO note about directly working with PMU after I read on that

---------
Resources
---------

* **Cortex-A Series Programmer's Guide for ARMv7-A** (copy here)

  http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.den0013d/index.html

* **Cortex-A Series Programmer's Guide for ARMv8-A** (copy here)

  https://static.docs.arm.com/den0024/a/DEN0024.pdf

* Jason D. Bakos: **Embedded Systems: ARM Programming and Optimization**

* Chris Simmonds: **Mastering Embedded Linux Programming**

* Agner Fog: **Optimizing Software in C++** (the first volume)

  http://www.agner.org/optimize/optimizing_cpp.pdf

  Written for x86, but most of this applies to ARM **more so**.


------------------
Interesting boards
------------------

(You know RPi1/2/3 - not interesting)

---------------------
(router) Banana PI R1
---------------------

.. image:: bpi-r1.png
   :width: 80%
   :align: center

------------------------------
(router) SolidRun ClearFog Pro
------------------------------

.. image:: clearfog-pro.jpg
   :width: 80%
   :align: center


---------------------------------------
(network-MiniITX) SolidRun MACCHIATOBin
---------------------------------------

.. image:: macchiato.jpg
   :width: 80%
   :align: center

-----------------------------
(mini-powerhouse) Odroid XU-4
-----------------------------

.. image:: odroid-xu4.jpg
   :width: 100%
   :align: center

-----------------------------
(RPi4x2+GbE) NanoPi M3
-----------------------------

.. image:: nanopi-m3.jpg
   :width: 60%
   :align: center

--------------
Dragonbox Pyra
--------------

.. XXX this

TODO picture

-----
Links
-----


* http://www.cnx-software.com/

* https://www.reddit.com/r/linux_devices

* https://www.reddit.com/r/arm


--------
Ad no. 2
--------

Looking for people in near future

* C++
* Linux
* ARM

* Low level high-performance networking stuff
* Focus on '**high-performance**' more than 'networking'

* ``strace``, ``valgrind``, ``perf`` are good

``ferdinand.majerech[AT]eset.sk``


-------
Contact
-------

* https://gitlab.com/kiith
* https://github.com/kiith-sa
* http://defenestrate.eu
* ``kiithsacmp[AT]gmail.com``

-------
The End
-------

(v01d)
