:css: slides2.css

.. TODO on v01d, write a series of articles about internet freedom/privacy
.. TODO implement the v01d pleroma. Do it on a RPi Zero.
.. TODO discuss this talk for NamakanyDen, probably not worth it,
        but we *may* have a local hacker pleroma ready for that!
.. CURRENTLY 20 SLIDES - 5 OVER LIMIT
.. LIMITS: 15 slides for this part!
.. LIMITS: total < 580 lines (ideally <500)

Fediverse a staronové Internety
===============================



.. Internet pred Googlom
.. =====================
.. 
.. * Usenet
.. * IRC
.. * Mail
.. * Fóra
.. 
.. - Moderovanie ľuďmi
.. - Pseudonymita
.. - Nekontrolovaná **nelokálna** komunikácia
.. - Nekonzistentnosť, blikajúce texty a menej 'Serious business'
.. 
.. 
.. -----

Fediverse - federácia sociálnych sietí
======================================

.. image:: fediverse.png
   :class: fgimg-right

* Profily, posty, videá, obrázky, following, atď atď

* **decentralizácia** (*federácia*) - ako email

  - Fediverse nevlastní žiadna firma alebo skupina

  - Ktokoľvek môže založiť server a zapojiť ho do siete

  - Servery môžu byť odlišné 'siete/služby' (príklad Youtube/Twitter)



----

Fediverse - servery
===================

.. NOTE have a raspberry in hand

.. image:: piserver.jpg
   :class: bgimg
   :width: 100%

* Sieť tvoria súkromné servery (*instance*)

  - Jednotlivci, komunity, spoločnosti

  - (na Raspberry sa vojde 100+ ľudí)

  - Veľké servery bývajú crowdfundované (napr. cez Patreon)

* Server administruje jeho tvorca/komunita, **nie stroj**
* Server software je open source - auditovateľný

----

Fediverse - sieť
================

.. image:: mastodon-federation.gif
   :class: fgimg-center
   :width: 50%


* Servery komunikujú spoločným *protokolom* (**ActivityPub**)

  - Používatelia môžu followovať/odpovedať/atď medzi službami
  - [Príklad: odpoveď na Youtube video na Twitteri]

* Používatelia sa followujú medzi servermi - servery sa prepájajú

* Z prepojení vzniká jednotný feed celej siete (*global timeline*)

----

Príklad - Mastodon
==================

.. image:: mastodon-logo.png
   :class: fgimg-right
   :width: 20%

* "Open source Twitter"

* Najúspešnejšia federovaná sociálna sieť

* 500 000 *aktívnych* používateľov

[How it works video]

* https://joinmastodon.org [demo 02:35, mastodon.technology?]


----

Mastodon
========

.. image:: mastodon.png
   :class: bgimg
   :width: 100%

* Microblogging: "Twitter na steroidoch"

* Local timeline vs federated timeline

* Nástroje na export/import/odstránenie dát

* Aplikácie pre Android/IOS, Linux/OSX/Windows/atď [demo]

  - https://joinmastodon.org/apps

----

Mastodon - kultúra
==================

.. image:: joinmastodon.jpg
   :class: bgimg
   :width: 100%

* Otvorená komunikácia
* Komunikácia > propagácia
* Malé, osobné komunity namiesto veľkého servera
* Je dobré byť divný
* Moderácia ľuďmi a izolácia škodlivých serverov (napr náckovia)
* Nástroje proti obťažovaniu / trolovaniu

----

Mastodon - servery
==================

.. image:: laughingman.png
   :class: bgimg-weak
   :width: 100%

============================== ==================================
============================== ==================================
https://mastodon.social        prvý server [uzavreté registrácie]
https://mstdn.io               všeobecný server
https://mastodon.technology    tech nadšenci
https://mastodon.art           grafici
https://tabletop.social        hráči doskových hier
https://niu.moe                anime/manga komunita
https://mastodon.gamedev.place herní vývojári
https://mathstodon.xyz         matematici
...                            ...
...                            ...
...                            ...
https://mastodon.sk            neodporúčam
============================== ==================================

.. ``noagendasocial.com``       anarchokapitalisti
.. ``soc.ialis.me``             anarchokomunisti
.. ``social.theliturgists.com`` kresťania

viac na https://the-federation.info

Je bežné byť na viacerych serveroch naraz - aplikácie to podporujú


----

Iné "siete" vo Fediverse
========================

.. NOTE show each of these

.. |lp| image:: pleroma-16.png

.. |lt| image:: peertube-16.png

.. |lP| image:: pixelfed-16.png

.. |lw| image:: writefreely-16.png

.. |lf| image:: friendica-16.png

.. |ld| image:: diaspora-16.png

===================== ======================================================= ========================== =========
===================== ======================================================= ========================== =========
|ld| ``Diaspora``     Prvá federovaná sociálna sieť                           https://pluspora.com       Stable
|lp| ``Pleroma``      'Twitter', /Anime komunita                              https://pleroma.soykaf.com Beta
|lf| ``Friendica``    Experimentálny facebookoid                              https://social.isurf.ca    Beta
|lt| ``PeerTube``     'Youtube'                                               ...                        Alpha
|lP| ``PixelFed``     'Instagram' (?)                                         ...                        Pre-alpha
|lw| ``Write Freely`` Blogovacia platforma                                    ...                        Alpha
``...``               ...                                                     ...                        ...
===================== ======================================================= ========================== =========



----

Prečo
=====

* Decentralizovaná sieť nemôže skrachovať, byť predaná, zmeniť vedenie
* Pod kontrolou používateľov

  - Otvorený kód - bez skrytého svinstva
  - Bez zberu dát, tracker cookies, počúvajúcich aplikácií
  - Pseudonymita *alebo* "reálna" identita
  - Zmazať znamená zmazať
* Žiadne reklamy, žiadna analytika 

----

Catch-22
========

Ľudia nechcú ísť mimo Facebook (Twitter,Youtube) lebo tam nie sú ľudia

Takže ľudia nie sú mimo Facebook

----

Mimo Fediverse
==============

.. image:: ssb.png
   :class: fgimg-right

* Secure Scuttlebutt

  - post-apo mesh sieť
  - bez serverov (peer-to-peer)
  - bezpečná sieť

* Rôzne blockchain-based siete

  - Blockchain je cool

----

Súkromná komunikácia - Mail
===========================

.. image:: tutanota.png
   :class: fgimg-right
   :height: 180px

* ``tutanota.com`` (DE)

  - User-friendly podmienky, kompletne open-source
  - Šifrovanie posielaných mailov cez dohodnuté kľúče

* ``protonmail.com`` (CH)

  - Čiastočne open source
  - Šifrovanie posielaných mailov cez PGP

.. image:: protonmail.png
   :class: fgimg-right
   :height: 180px

Idea:

* Šifrovanie pred zápisom na server
* Iba používateľ vie čítať (asymetrické crypto)
* Open-source klient - auditovateľnosť
* Za nadštandard sa platí - nie dátami

----

.. Súkromná komunikácia - Chat
.. ===========================
.. 
.. * Matrix
.. 
.. .. XXX problems with Telegram, Signal, Wire?, WhatsApp, etc.
.. 
.. ----

Googlenie 'bez googlu'
======================

* ``searx.me`` - proxy/agregátor search enginov

  - Open source, dá sa spustiť vlastný server

* ``duckduckgo.com``

* Decentralizované search enginy... work in progress

* Stále nie je dobrá alternatíva nezávislá od Google

----

Prehliadanie bez sledovania
===========================

.. |dis| image:: disconnect.png
   :width: 32px

.. |ubo| image:: ublock-origin.png
   :width: 32px

.. |uma| image:: umatrix.png
   :width: 32px

Google Chrome robí Google

* Google nemá záujem dať priestor súkromiu

.. image:: firefox.png
   :class: fgimg-right

Mozilla Firefox

* Nedávno prekopali grafický kód
* Od leta má blokovať **všetky** tracker cookies
* Addony (https://addons.mozilla.org): [demo]

  - |dis| ``Disconnect`` - vypne trackery, zrýchli prehliadač
  - |ubo| ``UBlock Origin`` - blokuje reklamy, zrýchli prehliadač
  - |uma| ``UMatrix`` - detailná kontrola, zrýchli prehliadač (ale)

``AdNauseam``: https://adnauseam.io/ - sabotáž reklamného business modelu

----

Anonymita
=========

.. image:: tor.png
   :class: fgimg-right
   :width: 250px

* VPN
* Tor: https://torproject.org/projects/torbrowser.html.en
* I2P
* Freenet (hardcore)


----

Retro
=====

.. image:: town.png
   :class: fgimg-center
   :width: 90%

* Tilde komunity: https://tilde.town, https://tildeverse.org/members/

* BBS: https://telnetbbsguide.com

* IRC: https://freenode.net

* https://neocities.org/browse

----

Používajte mozog
================

* Nepostujte verejne čísla účtov/heslá/atď
* Nepripájajte práčku na internet
* Ani topánky

...

* Prečkajte prvé IOT prúsery

.. image:: gdpr-fridge.jpg
   :class: bgimg-right
   :width: 600px

----

Viac informácií
===============

.. image:: disroot.png
   :class: fgimg-right
   :width: 400px

* https://switching.social [demo]

* https://fediverse.party [demo]

  - Štatiskiky (tých serverov, čo súhlasia so štatistikami)

* https://disroot.org [demo?]

----

V01D
====

Košický hackerspace

https://v01d.sk

``Skladná 58, Košice``

* Dvojtýždenné meetupy (najbližšie ``utorok 12.03 o 18:00``):

  - Kozmonautika, digitálne umenie, CPU architektúry, hardware hacking, open source,
    bezpečnosť, sci-fi, gamedev...

* Projekty:

  - Herná archeológia (SMAC, ScummVM)

  - 3D tlačiareň

  - Cyklistická časomiera

  - Mydlo

  - Pec na tavbu hlinníka?

.. image:: v01d.svg
   :class: bgimg

----

The End
=======

Internetové korporácie sú ako ušľachtilé syry; každá je jedinečná a plná plesne


.. knowledge generated from your home should belong to you, and *you*
   should profit from it. It should not be *implicitly* given to
   Google/Facebook/etc. to make money from and you do *not* need to hand
   this information over in order to use it (for apps, services, whatever)
