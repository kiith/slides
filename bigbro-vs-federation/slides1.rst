:css: slides1.css

Intro
=====

**Ferdinand Majerech**

V01D hackerspace, https://v01d.sk

Open source vývoj, sieťová / IOT bezpečnosť v ESET

Slides: https://gitlab.com/kiith/slides/tree/master/bigbro-vs-federation

----

.. Internet pred Googlom
.. =====================
.. 
.. * Usenet
.. * IRC
.. * Mail
.. * Fóra
.. * Ale aj centralizované: AOL, Compuserve, atď
.. 
.. - Moderovanie ľuďmi
.. - Pseudonymita
.. - Menej
.. - Nekonzistentnosť, blikajúce texty a menej 'Serious business'
.. 
.. 
.. 
.. ----


Google - prvé roky (<2001)
==========================

.. image:: google.png
   :class: fgimg-right
   :width: 400px

* Najlepší vyhľadávací produkt
* Žiadny business model, neustále straty
* Odpor voči reklamám

.. code::

   “We expect that advertising funded search engines will be inherently biased
   towards the advertisers and away from the needs of the consumers. This type of
   bias is very difficult to detect but could still have a significant effect on
   the market... we believe the issue of advertising causes enough mixed
   incentives that it is crucial to have a competitive search engine that is
   transparent and in the academic realm.”
   - Larry Page, “The Anatomy of a Large-Scale Hypertextual Web Search Engine"

* Secret sauce: využiť vstup používateľov na zlepšenie výsledkov

----

Kríza a prežitie
================

.. image:: google-income.png
   :class: fgimg-right
   :width: 400px

* Dot-com crash
* Investori v panike
* Otočenie business modelu

* Cielené reklamy, pravdepodobnosť kliknutia

* Profit!

----

Behaviorálny marketing
======================

* Data exhaust > "Machine Intelligence" > Predpoveď správania
* "Akú šancu mám, že **tento** user klikne na **túto** reklamu?"
* Viac dát > lepšie predpovede > spokojnejší zákazníci > peniaze > dáta ...

* Shoshana Zuboff: vznik "Behaviorálneho trhu"

----

Collect ALL the data
====================

* Kamery, mikrofóny, senzory, **disky** sú lacné

.. code::

  "Every new 'product' provides a new way of collecting behavioral surplus
  Each is a slightly different combination of hardware, software and sensors;
  in shape of a T-shirt, glasses, a phone, a vacuum, body monitor implant,
  etc; each sharing the single purpose: behavioural surplus capture"
  - Shoshana Zuboff, The Age of Surveillance Capitalism

* Jediný kto súkromie potrebuje je... Google/FB/atď

  - Aby nebol zber dát ohrozený verejným znepokojením
  - Je ľahšie žiadať o odpustenie ako o povolenie (Google wardriving)

----

.. image:: clown-computing.jpg
   :class: fgimg-center
   :width: 100%

----

Prediktívne produkty
====================

* Kúpite tento produkt? (Google/FB)
* Stavíte sa na záchode cestou do práce? (Google/FB)
* Budete voliť túto stranu? (CA, niekedy Google)
* Bude tento zamestnanec robiť problémy? (Palantir, CA)

Zákazníci:

- Reklama, politické strany
- Tajné služby, silové zložky, PMC
- Poisťovne, bankový sektor...

Obama 09 / Schmidt - kto sa dá presvedčiť?

----

Sociálna modifikácia
====================

* Osobní asistenti - efektívnejšie reklamy
* Cielená komunikácia, boti, cielený obsah
* Vytváranie informačnej bubliny (cielene alebo nie)
* Nepriamy skupinový nátlak
* Gamifikácia správania?

----

Príklad: Social Credit (!= Sesame Credit)
=========================================

.. image:: pooh.jpg
   :class: bgimg

* Veľa bullshitu v médiách
* Integrácia existujúcich zdrojov dát (soc.siete, verejné kamery, nákupy...)
* Žiadne "jedno skóre"
* Firemné blacklisty
* Zatiaľ len ak je človek odsúdený

https://www.chinalawtranslate.com/seeing-chinese-social-credit-through-a-glass-darkly/

Chaos Communication Congress


----

Príklad: Cambridge Analytica
============================

.. image:: cambridge-analytica.png
   :class: fgimg-right


* Dáta od FB
* Obsah/komunikácia personalizovaná pre každého voliča
* India, Mexiko, Donald Trump, Brexit

* Čoskoro vo vašich obľúbených voľbách, ako **emerdata**

* 5% stačí

[video]

https://en.wikipedia.org/wiki/Cambridge_Analytica

https://www.theguardian.com/news/series/cambridge-analytica-files

----

Príklad - Palantir
==================

.. image:: palantir.png
   :class: fgimg-right
   :width: 400px

* "reduce terrorism while preserving civil liberties"


* Zákazníci: tajné služby, armáda (irak)

  - neskôr polícia - "crime predicting"
  - dnes aj banky, hedge fundy, poisťovne, firmy

* Dáta od tretích strán, z policajných CCTV sietí, otvorených datasetov atď.

.. code::

   "Aided by 120 “forward-deployed engineers” of Palantir during 2009, Peter
   Cavicchia III of JPMorgan used Metropolis to monitor employee communications
   and alert the insider threat team when an employee showed any signs of
   potential disgruntlement ... ...  The Metropolis team used emails, download
   activity, browser histories, and GPS locations from JPMorgan owned smartphones"
   - Mak, Aaron - Bloomberg


https://en.wikipedia.org/wiki/Palantir_Technologies

https://www.theguardian.com/world/2017/jul/30/palantir-peter-thiel-cia-data-crime-police

----

New and Improved
================

.. image:: roomba.jpg
   :class: fgimg-right
   :width: 300px

* IOT vyrieši všetky problémy ktoré nemáte
* IRobot privacy scandal (2017)

  - Zber máp bytov/domov používateľov
  - "The company said it might **share** mapping data *with customers’ consent*, not *sell* it."

https://www.nytimes.com/2017/07/25/technology/roomba-irobot-data-privacy.html

----

Coming soon
===========

.. image:: body.png
   :class: fgimg-right
   :width: 400px

* **Internet of Body**
* Pancreas XL - with Android IOT 2.0
* Efektívnejšia manipulácia (Deepfakes, generovaný text)

  - Hlas podobný komu len chcete

----

Sledovanie by default
=====================

Google/FB odstránili *etickú normu* súkromia

* Ak firma *môže* dáta zbierať, tak to robí

  - Nie je motivácia to nerobiť
  - Ľahko sa dá ospravedlniť prečo je to vlastne dobré
* "Sľubujeme že dáta nepredávame"

  - To neznamená že ich nemeníme na produkty
  - Budúci management / majiteľ firmy bude mať iný názor

.. * Ak službe **nehrozí drastický postih**, tak dáta zbiera
.. Pri IOT je dôvodom často lenivosť
.. 
.. * Ale keď už dáta mám, prečo ich nevyužiť... ?

----

TLDR
====

* Nie ste produkt
* Ste zdroj materiálu (dát)
* Produkt sú budúce rozhodnutia vás a masy ľudí

  - Kliknúť na reklamu
  - Kúpiť viac hernej meny
  - Dať hlas doc. JuDr. kpt. ; Csc.
  - Báť sa mexičanov/židov/afričanov/američanov/rusov
  - Nezjesť ten steak (poisťovňa)
  - Ísť po tejto ulici (po tamtej práve berú Ujgurov do koncentráku)


----

Zdroje/Viac informácií
======================


* La Quadrature du Net: https://www.laquadrature.net/en/
* Electronic Privacy Information Center: https://epic.org/
* Aral Balkan, Laura Kalbag: https://ar.al/, https://ind.ie/
* The Creepy Line: https://www.thecreepyline.com/
* Laura Poitras: Citizenfour
* Shoshana Zuboff: ``The Age of Surveillance Capitalism ...``
* Glenn Greenwald
