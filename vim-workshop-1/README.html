<h1 id="vim---the-language-of-text-editing">VIM - the language of text editing</h1>
<h2 id="vim">Vim</h2>
<ul>
<li><code>vim</code> is a text editor.</li>
<li><code>vim</code> is a <strong>language for text editing</strong>.</li>
<li><code>vim</code> is a framework for extending its own <code>vim</code> language and introducing new text editing paradigms (<strong>next time</strong>).</li>
<li><code>vim</code> is <strong>hard to learn</strong></li>
<li><code>vim</code> is <strong>impossible to master</strong> - there are always new ways to improve</li>
</ul>
<p>Vim is about finding the most efficient, concise way to change text; with the least keystrokes, least thinking, and <strong>no</strong> mouse. Good ergonomy is a side-effect.</p>
<p>People often go into Vim trying to turn it into their favorite IDE; or into notepad. This workshop is not about that; it is about using Vim the way it was designed to be used.</p>
<h2 id="preparation">Preparation</h2>
<p>I expect you <strong>have some sort of Vim installed</strong>. Substitute <code>vim</code> for whatever Vim flavour/frontend you like (e.g. <code>gvim</code>, <code>nvim</code> or plain old <code>vim</code>).</p>
<ol>
<li><p>Create file <code>test.c</code> with following contents. Back it up, it will be used in more exercises.</p>
<div class="sourceCode"><pre class="sourceCode c"><code class="sourceCode c"><span class="ot">#include &lt;stdio.h&gt;</span>

<span class="dt">int</span> main(<span class="dt">int</span> argc, <span class="dt">char</span>* argv[])
{
    printf(<span class="st">&quot;&lt;div id=&#39;parent&#39;&gt;&lt;div id=&#39;redundant&#39;&gt;&lt;p&gt;Hello World! %d&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;</span><span class="ch">\n</span><span class="st">&quot;</span>, argc);
    <span class="kw">return</span> <span class="dv">0</span>;
}</code></pre></div></li>
<li>Also have a larger text file <em>that you can destroy</em> ready for editing. I recommend some sort of code.</li>
</ol>
<h2 id="constant-improvement">Constant improvement</h2>
<p><strong>Refactor your text editing</strong> - with the <em>normal mode language</em>, <em>mappings</em>, <em>command mode</em>, <em>macros</em>, <em>scripts</em>...</p>
<p>Every (day, week, month):</p>
<ol>
<li>Find a complex/repetitive thing you do</li>
<li>Find a simpler/non-repetitive way to do it</li>
<li>Write it down - and stick to your monitor/table/whatever</li>
<li>Make it a habit</li>
<li>Repeat</li>
</ol>
<h2 id="vim-language---normal-mode">Vim language - normal mode</h2>
<p>Called &quot;normal&quot; mode because it's what you <strong>normally</strong> use.</p>
<p>We operate on <em>text</em>, <strong>not</strong> characters.</p>
<p>Syntax: <strong>verbs</strong>, <em>counts</em> and <strong>nouns</strong>: <code>c2w</code>:</p>
<ul>
<li><p><code>c</code> means <em>change</em> (rewrite); it is a <strong>verb</strong>; an action</p>
<p><code>c</code> enters the Notepad-like 'insert mode', which we use for <em>typing</em>, <strong>not</strong> for editing. It is exited by <code>&lt;Esc&gt;</code>.</p>
Stay in insert mode for as short as possible.</li>
<li><code>2</code> is an <em>optional</em> <strong>count</strong></li>
<li><code>w</code> means <em>word</em>; it is a <strong>noun</strong></li>
<li><code>c2wSTUFF&lt;Esc&gt;</code>: &quot;change 2 words into 'STUFF'&quot;</li>
<li><code>cwSTUFF&lt;Esc&gt;</code>: &quot;change word into 'STUFF'&quot;</li>
<li><code>w</code>: &quot;move to next word&quot;</li>
</ul>
<h2 id="moving-around">Moving around</h2>
<p>We're operating on <em>text</em>, <strong>not</strong> characters. We <strong>do not</strong> 'move left/right'.</p>
<p>Nouns:</p>
<ul>
<li><code>w</code>: Next <strong>word</strong>. <code>www</code> or <code>3w</code> moves 3 words</li>
<li><code>b</code>: Go <strong>back</strong> a word. <code>3b</code> moves back 3 words</li>
<li><code>fx</code>: <strong>Find</strong> character <strong>x</strong> forward/backward on the line. <code>f;</code> moves to the next semicolon.</li>
<li><code>tx</code>: Go <strong>till</strong> character <strong>x</strong>. Like <code>f</code>, but <em>before</em> the character. <code>ct;</code> changes till a semocolon, <strong>without touching it</strong>.</li>
<li><code>Fx</code>/<code>Tx</code> are like <code>fx</code>/<code>tx</code> but backward</li>
<li><code>j</code>/<code>k</code>: Down/up one line. <code>5j</code> moves down 5 lines. <code>c5k</code> changes 5 lines <em>upwards</em></li>
<li><code>/thing&lt;Enter&gt;</code>: Search for string <strong>thing</strong> from the current position. <code>c/end&lt;Enter&gt;</code> will change up to, not including, <strong>end</strong>.</li>
</ul>
<p>Hint: use <code>j</code>/<code>k</code> efficiently with <a href="http://jeffkreeftmeijer.com/2013/vims-new-hybrid-line-number-mode/">relative line numbering</a></p>
<p>Nouns:</p>
<ul>
<li><code>h</code>/<code>l</code>: Left/right one character. <em>inefficient</em>, <strong>use <code>f</code>/<code>w</code>/etc instead</strong>.</li>
</ul>
<p>Arrow keys also exist for those who enjoy repetitive strain injury pain.</p>
<p>More sugar:</p>
<ul>
<li><code>$</code>/<code>^</code>: go to end/beginning of line</li>
<li><code>gg</code>: <strong>go to line</strong> specified by count (0 by default)</li>
<li><code>G</code>: go to end of file</li>
</ul>
<h2 id="exercise-1">Exercise 1</h2>
<p>Open a random text file (<em>that can be destroyed</em>) and try moving around.</p>
<p>What do these movements mean? Test them:</p>
<ol>
<li><code>10j</code></li>
<li><code>/ipsum&lt;Enter&gt;</code></li>
<li><code>t;</code></li>
<li><code>7fe</code></li>
<li><code>F,</code></li>
<li><code>bbb</code></li>
<li><code>3b</code></li>
<li><code>G</code></li>
</ol>
<p>Try your own combinations.</p>
<h2 id="exercise-1b">Exercise 1b</h2>
<p>Now try each of these movements with <code>c</code> (<code>&lt;ESC&gt;</code> to get back to normal mode)</p>
<p>Try your own combinations.</p>
<h2 id="repeating-undoing">Repeating &amp; undoing</h2>
<ul>
<li><code>.</code>: repeat last action.</li>
<li><code>u</code>: undo last action.</li>
<li><code>n</code>/<code>N'': repeat a</code>/`` search forwards, backwards</li>
</ul>
<p>Example (demo): <code>ct,</code> to change till a comma. Move around the code and <code>.</code> to repeat. Then use <code>u</code> repeatedly to undo the changes.</p>
<p>Vim sentences are <em>repeatable</em> and <em>undoable</em></p>
<h2 id="exercise-2">Exercise 2</h2>
<p>Try repeating movements from Exercise 1b with <code>.</code> and undoing with <code>u</code>.</p>
<p><code>.</code> and <code>u</code> are still part of Vim language. Try using counts, such as:</p>
<ul>
<li><code>5.</code></li>
<li><code>100u</code></li>
</ul>
<p>Assume your file contains a the word <strong>BAD</strong> in multiple places. What does this do?</p>
<ul>
<li><code>/BAD&lt;Enter&gt;cwFIXED&lt;Esc&gt;n.n.n.</code></li>
</ul>
<p>This can be used for more than search/replace since we work on <em>objects</em>, not characters.</p>
<h2 id="text-objects">Text objects</h2>
<p>Nouns: Motions (<code>w</code>, <code>fx</code>, etc.) and <strong>Text objects</strong>:</p>
<p>Example: <code>ci&quot;</code>, <code>ca&quot;</code></p>
<ul>
<li><code>c</code>: change</li>
<li><code>i</code>: in, <code>a</code>: around</li>
<li><code>&quot;</code>: double quotes</li>
</ul>
<p><code>ci&quot;</code>: &quot;change in double quotes&quot;, <code>ca&quot;</code>: &quot;change around double quotes&quot;</p>
<p>Try using this!</p>
<p>Nouns:</p>
<ul>
<li><code>ib</code>/<code>ab</code>: in/around <strong>block</strong> (parentheses)</li>
<li><code>iB</code>/<code>aB</code>: in/around <strong>Block</strong> {braces}</li>
<li><code>it</code>/<code>at</code>: in/around <strong>tags</strong> &lt;b&gt;tags&lt;/b&gt;</li>
<li><code>i&quot;</code>/<code>a&quot;</code>: in/around <strong>double quotes</strong></li>
<li><code>i'</code>/<code>a'</code>: in/around <strong>single quotes</strong></li>
<li><code>is</code>/<code>as</code>: in/around <strong>sentence</strong></li>
<li><code>ip</code>/<code>ap</code>: in/around <strong>paragraph</strong></li>
<li><code>iw</code>/<code>aw</code>: in/around <strong>word</strong></li>
<li>etc.</li>
</ul>
<p>Quickly edit strings, function parameters, scopes, as well as plain text.</p>
<h2 id="exercise-3">Exercise 3</h2>
<p>What do the following sentences mean? Try them out:</p>
<ol>
<li><code>cib</code></li>
<li><code>c2ib</code></li>
<li><code>cab</code></li>
<li><code>ciB</code></li>
<li><code>cas</code></li>
<li><code>cip</code></li>
</ol>
<p>Try your own combinations.</p>
<h2 id="verbs">Verbs</h2>
<ul>
<li><code>d</code>: <strong>delete</strong> (cut in other editors). <code>dip</code> deletes the paragraph.</li>
<li><code>y</code>: <strong>yank</strong> (copy). <code>yib</code> copies contents of parens</li>
<li><code>c</code>: <strong>change</strong> <code>ci&quot;</code> changes the contents of a string</li>
<li><code>p</code>: <strong>paste</strong> <code>2p</code> pastes twice.</li>
<li><code>v</code>: <strong>visually</strong> select; (then use motions and verbs to work on selected code)</li>
<li><code>&gt;</code>/<code>&lt;</code>: <strong>indent</strong>/dedent. <code>&gt;iB</code> indents the block.</li>
<li><code>=</code>: <strong>format</strong> <code>=iB</code> formats the block.</li>
</ul>
<p>Visual selection is a separate topic; leaving that for future.</p>
<p>Extra sugar:</p>
<ul>
<li><code>yy</code> copies current line</li>
<li><code>dd</code> deletes (cuts) current line</li>
<li><code>&gt;&gt;</code>/<code>&lt;&lt;</code>: indent/dedent/format current line</li>
<li><code>C</code> changes till end of line</li>
</ul>
<h2 id="exercise-4">Exercise 4</h2>
<p>What do the following combinations mean? Try them:</p>
<ol>
<li><code>d$</code></li>
<li><code>das</code></li>
<li><code>ct;</code></li>
<li><code>cit</code></li>
<li><code>5dd5p</code></li>
<li><code>gg=G</code></li>
<li><code>ggyG2p</code></li>
<li><code>&gt;5j</code></li>
<li><code>5&gt;&gt;</code></li>
<li><code>&gt;iB</code></li>
</ol>
<p>Try your own blah blah.</p>
<h2 id="ergonomics-speed-cost">Ergonomics, speed, cost</h2>
<p>TLDR: <strong>Minimize hand movement</strong></p>
<p>Your hands are easy to fuck up.</p>
<p><img src="keyboardmap.png" alt="image" /></p>
<p>Primary hand-killers, from worst to best:</p>
<ol start="10" style="list-style-type: decimal">
<li>move hand to <em>mouse</em>, click, move mouse, move back to keyboard (AKA: selection)</li>
<li><p><em>touchpad</em> - same as mouse, shorter distance, more exhausting cursor movement</p>
<p>Better: <strong>kill the mouse</strong>, use <strong>trackpoint</strong> (no hand movement) if unavoidable</p></li>
<li><p><em>numpad</em>, (<em>arrow keys</em>) requires moving hands away from home row</p>
<p>Better: <strong>use number row, avoid one-char movement</strong></p></li>
<li><p><em>chords</em>, e.g. <code>&lt;Ctrl-A&gt;</code> and such. Includes <code>&lt;Shift&gt;</code> for uppercase characters.</p>
<p>Better: <strong>chord with one key per hand, avoid 3-key chords, remap common chords</strong></p></li>
<li><p><em>top row</em> (F-keys)</p>
<p>Better: <strong>swap CapsLock and ESC, minimize F-key usage</strong></p></li>
<li><p>All rows except asdfghjkl (aka home row)</p>
<p>Better: <strong>DVORAK, COLEMAK, Workman</strong></p></li>
</ol>
<p>0.8. <em>Home row</em> (asdfghjkl) + <code>&lt;Space&gt;</code></p>
<blockquote>
<p>Better: <strong>Direct brain-computer interface</strong></p>
</blockquote>
<p>Call this <strong>cost</strong> and use it to measure typing difficulty.</p>
<h2 id="exercise-5">Exercise 5</h2>
<ol>
<li>Open <code>test.c</code> from preparation.</li>
<li><p>Make the following change, using <strong>normal mode</strong> only.</p>
<p>I.e; do not use <code>c</code> (or <code>i</code>).</p>
<p>(Disclaimer: normal mode only is not optimal for rewriting a word)</p>
<p><strong>Find the cheapest sequence of keypresses necessary for this change. Write it down. Win imaginary points!</strong></p>
<p>Calculate cost by adding up costs for all keypresses and chords.</p>
<p>Change this:</p>
<p><code>printf(&quot;&lt;div id='parent'&gt;&lt;div id='redundant'&gt;&lt;p&gt;Hello World! %d&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;\n&quot;, argc);</code></p>
<p>to this (remove the redundant tag):</p>
<p><code>printf(&quot;&lt;div id='parent'&gt;&lt;p&gt;Hello World! %d&lt;/p&gt;&lt;/div&gt;\n&quot;, argc);</code></p></li>
<li>Type <code>:w exercise5.c</code> to save the result</li>
</ol>
<h2 id="more-stuff-reference">More stuff / reference</h2>
<p>Verbs:</p>
<ul>
<li><code>gq</code>: <strong>Text-wrap</strong> lines to max line width. <code>gqip</code> will wrap current paragraph.</li>
<li><code>&lt;C-R&gt;</code>: <strong>Redo</strong></li>
<li><code>x</code>: <strong>Delete character under cursor</strong></li>
<li><code>rx</code>: <strong>Replace</strong> character under cursor with <strong>x</strong></li>
</ul>
<p>Nouns (motions):</p>
<ul>
<li><code>e</code>: Go to <strong>end</strong> of current word.</li>
<li><code>ge</code>: Go to end of <strong>previous</strong> word.</li>
<li><code>n</code>: repeat a <strong>/ search</strong></li>
<li><code>;</code>: repeat a <strong>f/t search</strong></li>
<li><code>0</code>: start of line, even <strong>before any indent</strong></li>
</ul>
<p>... aand many more, study Vim documentation</p>
<h2 id="normal-insert-mode-normal-visual-mode">Normal + insert mode, normal + visual mode</h2>
<p><strong>Insert mode</strong>: works like traditional text editors.</p>
<p>Besides the <code>c</code> verb, it can be entered with:</p>
<ul>
<li><code>i</code>/<code>a</code> before/after the current character</li>
<li><code>I</code>/<code>A</code> at the beginning/end of the current line</li>
<li><code>o</code>/<code>O</code> on a new line before/after the current line</li>
</ul>
<p><strong>Visual mode</strong>: edit a selection with motions, perform action with a verb.</p>
<p>Workflow:</p>
<ol>
<li>enter with the <code>v</code> verb (e.g. <code>vib</code>)</li>
<li>edit the selection with motions (<code>w</code>, <code>fx</code>, etc.)</li>
<li>exit by typing a verb, which will apply to selection (<code>y</code>, <code>d</code>, <code>c</code>)</li>
</ol>
<p><strong>Block visual</strong> mode:</p>
<p>Allows editing multiple lines at once. Useful for <strong>commenting</strong>, <strong>column-wise copying</strong>, CSV, editing multiple HTML tags at once, etc.</p>
<p>Enter: <code>&lt;Ctrl-v&gt;</code></p>
<h2 id="exercise-6">Exercise 6</h2>
<p>What do these do? Try them:</p>
<ol>
<li><code>I#prefix#&lt;Esc&gt;</code></li>
<li><code>A#postfix#&lt;Esc&gt;</code></li>
<li><code>vibibd</code></li>
<li><code>oLorem Ipsum&lt;Esc&gt;</code></li>
<li><code>gg&lt;Ctrl-v&gt;GI#prefix#&lt;Esc&gt;</code></li>
<li><code>o#-&lt;Esc&gt;yy5pk&lt;Ctrl-v&gt;e5jye10p</code></li>
</ol>
<p>Again try your own blah...</p>
<h2 id="todo">TODO</h2>
<ul>
<li>TODO macros</li>
<li>TODO mappings</li>
<li>TODO refactoring own typing</li>
</ul>
<h2 id="how-to-quit-vim">How to quit Vim</h2>
<p>Google <em>How do I exit from Unreal Tournament?</em></p>
<ul>
<li><code>:x</code></li>
</ul>
<h2 id="cool-stuff">Cool stuff</h2>
<ul>
<li><code>:TOhtml</code>. Try it on current file.</li>
<li>(snippets, e.g. enum, sw)</li>
</ul>
<h2 id="next-time">Next time</h2>
<p><img src="v01d.png" alt="image" /></p>
<p><strong>Tuesday 14.11.2017 18:00</strong> Vim workshop 2 @ <strong>v01d</strong>: <em>Macros, mappings and language extension plugins</em></p>
<p><a href="https://www.v01d.sk">v01d.sk</a></p>
<p>IRC: <code>#v01d@FreeNode</code></p>
<p><strong>Národná Trieda 74, Snowden window</strong></p>
<p>Next meetup: <strong>this Tuesday, 18:00</strong></p>
<p>Future:</p>
<ul>
<li>Autocompletion, its many forms and plugins</li>
<li>Using command + normal mode for massive editing</li>
<li>More aggressive customization/plugins</li>
<li>Window and session management</li>
<li>NeoVim and not using VimScript</li>
</ul>
<h2 id="see-also">See also</h2>
<ul>
<li>The <code>vimtutor</code> command (comes with <code>vim</code>).</li>
<li>neovim.io : NeoVim</li>
<li>kakoune.org : an editor with a completely different language</li>
</ul>
