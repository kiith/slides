==================================
VIM - the language of text editing
==================================


.. XXX vim for people who think things like vim are hard
.. XXX end: some cool vim video?

------------
Introduction
------------

I'm Ferdinand Majerech

Member of the **v01d** hackerspace: https://void.sk

Software engineer at ESET (network security)

Open source dev (https://gitlab.com/kiith, formerly https://github.com/kiith-sa)

C/C++/D/low-level-ish Linux dev

---
Vim
---

* ``vim`` is a text editor.

* ``vim`` is a **language for text editing**.

* ``vim`` is a framework for extending its own ``vim`` language and introducing new text
  editing paradigms (**next time**).

* ``vim`` is **hard to learn**

* ``vim`` is **impossible to master** - there are always new ways to improve

Vim is about finding the most efficient, concise way to change
text; with the least keystrokes, least thinking, and **no** mouse.
Good ergonomy is a side-effect.

People often go into Vim trying to turn it into their favorite IDE; or into
notepad. This workshop is not about that; it is about using Vim the way it
was designed to be used.

-----------
Preparation
-----------

I expect you **have some sort of Vim installed**. Substitute ``vim`` for whatever
Vim flavour/frontend you like (e.g. ``gvim``, ``nvim`` or plain old ``vim``).

#. Create file ``test.c`` with following contents. Back it up, it will be used
   in more exercises.

   .. code:: c

      #include <stdio.h>

      int main(int argc, char* argv[])
      {
          printf("<div id='parent'><div id='redundant'><p>Hello World! %d</p></div></div>\n", argc);
          return 0;
      }

#. Also have a larger text file *that you can destroy* ready for editing. I recommend some sort of code.


--------------------
Constant improvement
--------------------

**Refactor your text editing** - with the *normal mode language*,
*mappings*, *command mode*, *macros*, *scripts*...

Every (day, week, month):

#. Find a complex/repetitive thing you do

#. Find a simpler/non-repetitive way to do it

#. Write it down - and stick to your monitor/table/whatever

#. Make it a habit

#. Repeat


--------------------------
Vim language - normal mode
--------------------------

Called "normal" mode because it's what you **normally** use.

We operate on *text*, **not** characters.

Syntax: **verbs**, *counts* and **nouns**: ``c2w``:

* ``c`` means *change* (rewrite); it is a **verb**; an action

  ``c`` enters the Notepad-like 'insert mode', which we use for *typing*, 
  **not** for editing. It is exited by ``<Esc>``.

  Stay in insert mode for as short as possible.
* ``2`` is an *optional* **count**
* ``w`` means *word*; it is a **noun**

* ``c2wSTUFF<Esc>``: "change 2 words into 'STUFF'"
* ``cwSTUFF<Esc>``: "change word into 'STUFF'"
* ``w``: "move to next word"


-------------
Moving around
-------------

We're operating on *text*, **not** characters. We **do not** 'move left/right'.

Nouns:

* ``w``: Next **word**. ``www`` or ``3w`` moves 3 words
* ``b``: Go **back** a word. ``3b`` moves back 3 words
* ``fx``: **Find** character **x** forward/backward on the line. ``f;`` moves to the next semicolon.
* ``tx``: Go **till** character **x**. Like ``f``, but *before* the character. ``ct;`` changes till a semocolon, **without touching it**.
* ``Fx``/``Tx`` are like ``fx``/``tx`` but backward
* ``j``/``k``: Down/up one line. ``5j`` moves down 5 lines. ``c5k`` changes 5 lines *upwards*
* ``/thing<Enter>``: Search for string **thing** from the current position. ``c/end<Enter>`` will change up to, not including, **end**.

Hint: use ``j``/``k`` efficiently with `relative line numbering
<http://jeffkreeftmeijer.com/2013/vims-new-hybrid-line-number-mode/>`_

Nouns:

* ``h``/``l``: Left/right one character. *inefficient*, **use ``f``/``w``/etc instead**.

Arrow keys also exist for those who enjoy repetitive strain injury pain.

More sugar:

* ``$``/``^``: go to end/beginning of line
* ``gg``: **go to line** specified by count (0 by default)
* ``G``: go to end of file


----------
Exercise 1
----------

Open a random text file (*that can be destroyed*) and try moving around.

What do these movements mean? Test them:

#. ``10j``
#. ``/ipsum<Enter>``
#. ``t;``
#. ``7fe``
#. ``F,``
#. ``bbb``
#. ``3b``
#. ``G``

Try your own combinations.

-----------
Exercise 1b
-----------

Now try each of these movements with ``c`` (``<ESC>`` to get back to normal mode)

Try your own combinations.

-------------------
Repeating & undoing
-------------------

* ``.``: repeat last action.
* ``u``: undo last action.
* ``n``/``N'': repeat a ``/`` search forwards, backwards

Example (demo): ``ct,`` to change till a comma. Move around the code and ``.`` to
repeat. Then use ``u`` repeatedly to undo the changes.

Vim sentences are *repeatable* and *undoable*

----------
Exercise 2
----------

Try repeating movements from Exercise 1b with ``.`` and undoing with ``u``.

``.`` and ``u`` are still part of Vim language. Try using counts, such as:

* ``5.``
* ``100u``

Assume your file contains a the word **BAD** in multiple places. What does this do?

* ``/BAD<Enter>cwFIXED<Esc>n.n.n.``

This can be used for more than search/replace since we work on *objects*, not
characters.

------------
Text objects
------------

Nouns: Motions (``w``, ``fx``, etc.) and **Text objects**:

Example: ``ci"``, ``ca"``

* ``c``: change
* ``i``: in, ``a``: around
* ``"``: double quotes

``ci"``: "change in double quotes", ``ca"``: "change around double quotes"

Try using this!

Nouns:

* ``ib``/``ab``: in/around **block** (parentheses)
* ``iB``/``aB``: in/around **Block** {braces}
* ``it``/``at``: in/around **tags** <b>tags</b>
* ``i"``/``a"``: in/around **double quotes**
* ``i'``/``a'``: in/around **single quotes**
* ``is``/``as``: in/around **sentence**
* ``ip``/``ap``: in/around **paragraph**
* ``iw``/``aw``: in/around **word**
* etc.

Quickly edit strings, function parameters, scopes, as well as plain text.

----------
Exercise 3
----------

What do the following sentences mean? Try them out:

#. ``cib``
#. ``c2ib``
#. ``cab``
#. ``ciB``
#. ``cas``
#. ``cip``

Try your own combinations.

-----
Verbs
-----

* ``d``: **delete** (cut in other editors). ``dip`` deletes the paragraph.
* ``y``: **yank** (copy). ``yib`` copies contents of parens
* ``c``: **change** ``ci"`` changes the contents of a string
* ``p``: **paste** ``2p`` pastes twice.
* ``v``: **visually** select; (then use motions and verbs to work on selected code)
* ``>``/``<``: **indent**/dedent. ``>iB`` indents the block.
* ``=``: **format** ``=iB`` formats the block.

Visual selection is a separate topic; leaving that for future.

Extra sugar:

* ``yy`` copies current line
* ``dd`` deletes (cuts) current line
* ``>>``/``<<``: indent/dedent/format current line
* ``C`` changes till end of line


----------
Exercise 4
----------

What do the following combinations mean? Try them:

#. ``d$``
#. ``das``
#. ``ct;``
#. ``cit``
#. ``5dd5p``
#. ``gg=G``
#. ``ggyG2p``
#. ``>5j``
#. ``5>>``
#. ``>iB``

Try your own blah blah.


-----------------------
Ergonomics, speed, cost
-----------------------

TLDR: **Minimize hand movement**

Your hands are easy to fuck up.

.. image:: keyboardmap.png

Primary hand-killers, from worst to best:

10.  move hand to *mouse*, click, move mouse, move back to keyboard (AKA: selection)

10.  *touchpad* - same as mouse, shorter distance, more exhausting cursor movement

     Better: **kill the mouse**, use **trackpoint** (no hand movement) if unavoidable

4.   *numpad*, (*arrow keys*) requires moving hands away from home row

     Better: **use number row, avoid one-char movement**

4.   *chords*, e.g. ``<Ctrl-A>`` and such. Includes ``<Shift>`` for uppercase characters.

     Better: **chord with one key per hand, avoid 3-key chords, remap common chords**

2.   *top row* (F-keys)

     Better: **swap CapsLock and ESC, minimize F-key usage**

1.   All rows except asdfghjkl (aka home row)

     Better: **DVORAK, COLEMAK, Workman**

0.8. *Home row* (asdfghjkl) + ``<Space>``

     Better: **Direct brain-computer interface**

Call this **cost** and use it to measure typing difficulty.


----------
Exercise 5
----------

#. Open ``test.c`` from preparation.

#. Make the following change, using **normal mode** only.

   I.e; do not use ``c`` (or ``i``).

   (Disclaimer: normal mode only is not optimal for rewriting a word)

   **Find the cheapest sequence of keypresses necessary for this change.
   Write it down. Win imaginary points!**

   Calculate cost by adding up costs for all keypresses and chords.

   Change this:

   ``printf("<div id='parent'><div id='redundant'><p>Hello World! %d</p></div></div>\n", argc);``

   to this (remove the redundant tag):

   ``printf("<div id='parent'><p>Hello World! %d</p></div>\n", argc);``

#. Type ``:w exercise5.c`` to save the result

----------------------
More stuff / reference
----------------------

Verbs:

* ``gq``: **Text-wrap** lines to max line width. ``gqip`` will wrap current paragraph.
* ``<C-R>``: **Redo**
* ``x``: **Delete character under cursor**
* ``rx``: **Replace** character under cursor with **x**

Nouns (motions):

* ``e``: Go to **end** of current word.
* ``ge``: Go to end of **previous** word.
* ``n``: repeat a **/ search**
* ``;``: repeat a **f/t search**
* ``0``: start of line, even **before any indent**

... aand many more, study Vim documentation

------------------------------------------
Normal + insert mode, normal + visual mode
------------------------------------------

**Insert mode**: works like traditional text editors.

Besides the ``c`` verb, it can be entered with:

* ``i``/``a`` before/after the current character
* ``I``/``A`` at the beginning/end of the current line
* ``o``/``O`` on a new line before/after the current line

**Visual mode**: edit a selection with motions, perform action with a verb.

Workflow:

#. enter with the ``v`` verb (e.g. ``vib``)
#. edit the selection with motions (``w``, ``fx``, etc.)
#. exit by typing a verb, which will apply to selection (``y``, ``d``, ``c``)



**Block visual** mode:

Allows editing multiple lines at once. Useful for **commenting**, **column-wise copying**, CSV, editing multiple HTML tags at once, etc.

Enter: ``<Ctrl-v>``

----------
Exercise 6
----------

What do these do? Try them:

#. ``I#prefix#<Esc>``
#. ``A#postfix#<Esc>``
#. ``vibibd``
#. ``oLorem Ipsum<Esc>``
#. ``gg<Ctrl-v>GI#prefix#<Esc>``
#. ``o#-<Esc>yy5pk<Ctrl-v>e5jye10p``

Again try your own blah...

----
TODO
----

.. XXX an exercise/example of constant improvement somewhere, maybe not right here
       (take some common behavior, and successively optimize it)
.. XXX also need scrolling somewhere, line C-U, C-D

* TODO macros
* TODO mappings
* TODO refactoring own typing

---------------
How to quit Vim
---------------

Google *How do I exit from Unreal Tournament?*

* ``:x``

----------
Cool stuff
----------

* ``:TOhtml``. Try it on current file.
* (snippets, e.g. enum, sw)


.. XXX before end; a teaser; surround, args, comments, whatever else I use
       could use a gif or tmux script
.. XXX see: https://www.youtube.com/watch?v=wlR5gYd6um0
.. XXX newspost and calendar on v01d.sk!

---------
Next time
---------

.. image:: v01d.png

**Tuesday 14.11.2017 18:00** Vim workshop 2 @ **v01d**:
*Macros, mappings and language extension plugins*

`v01d.sk <https://www.v01d.sk>`_

IRC: ``#v01d@FreeNode``

**Národná Trieda 74, Snowden window**

Next meetup: **this Tuesday, 18:00**


Future:

* Autocompletion, its many forms and plugins
* Using command + normal mode for massive editing
* More aggressive customization/plugins
* Window and session management
* NeoVim and not using VimScript

--------
See also
--------

* The ``vimtutor`` command (comes with ``vim``).
* neovim.io : NeoVim
* kakoune.org : an editor with a completely different language
