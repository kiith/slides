* Builtin backdoors in... pretty much anything
   
  whether 'debugging', 'improving product', 'anti-terrorism' or honest espionage
* Vulnerabilities in pretty much everything else
* Spectre, Meltdown, ...
* Internet of vulnerable broken Things
* Intel ME, AMD PSP, ARM "Trust" zone

  **Trust** in HW is like **Democratic People's** in Republic of Korea

