#!/bin/bash

# XXX refactor with profiling-2 talk
shopt -s expand_aliases
alias type_keys='tmux send-keys'

START_SECTION=${1:-defaultValue}
REACHED_START=1
SESSION=TALK_FOSS

check_start_section () {
	if [ "$START_SECTION" == "$1" ]; then
		REACHED_START=0
	fi
	return $REACHED_START
}



# shows the first argument on the 'message pane'
show_big () {
    tmux select-pane -t 1
    tmux resize-pane -y 6
    tmux send-keys C-u "figlet -c -t -f small " "$1" " " C-m
    tmux select-pane -t 0
}

show () {
    tmux select-pane -t 1
    tmux resize-pane -y 2
    tmux send-keys C-u "        <<<<<< " "$1" " >>>>>>"
    tmux select-pane -t 0
}


section_intro () {
	check_start_section "intro" || return

    show_big "author, v01d"
    type_keys "mdless --no-pager v01d.md" C-m
    read -p "Enter to continue"

    show_big "privacy is cool again"
    type_keys "mdless --no-pager privacy.md" C-m
    read -p "Enter to continue"

    show_big "federation"
    type_keys "mdless --no-pager federation.md" C-m
    read -p "Enter to continue"

    show_big "security"
    type_keys "mdless --no-pager security.md" C-m
    read -p "Enter to continue"

    show_big "open CPU architectures"
    type_keys "mdless --no-pager opencpu.md" C-m
    read -p "Enter to continue"

    show_big "linux-friendly HW"
    type_keys "mdless --no-pager linuxhw.md" C-m
    read -p "Enter to continue"

    show_big "embrace, extend"
    type_keys "mdless --no-pager embraceextend.md" C-m
    read -p "Enter to continue"

    show_big "missing the train: VR, neural"
    type_keys "mdless --no-pager interfaces.md" C-m
    read -p "Enter to continue"

    show_big "links"
    type_keys "mdless --no-pager links.md" C-m
    read -p "Enter to continue"
}



# Start the actual code.

for (( i = 200; i > 0; i-- )); do
    echo ""
done
tmux kill-session -t $SESSION
tmux -2 new-session -d -s $SESSION
# tmux -2 attach-session -t $SESSION &
tmux split-window -v
tmux select-pane -t 1 -P 'bg=white'
tmux select-pane -t 1 -P 'fg=black'
tmux resize-pane -y 5
# type_keys "tmux resize-pane -D 5" C-m
tmux select-pane -t 0
# type_keys "cd ~" C-m

for (( i = 10; i > 0; i-- )); do
    show_big "$i"
    sleep 1
done

section_intro

show_big "Questions"
read -p "Enter to continue"
tmux kill-session -t $SESSION
