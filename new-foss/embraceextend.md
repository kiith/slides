**Microsoft embracing and extending OSS**

* WSL
* MS linux/OSS work motivated by Azure, developers (developers developers)
* Github bought by MS
* *Some* people moving to GitLab, self-hosted, ?federated? servers
* More corp influence in general, not just MS

Corporate OSS: Focus on developer ecosystem, **not on user freedom**

