* Private mail services

  - Protonmail: >5min users, increasing
  - Tutanota:   >2min users, increasing
* Private cryptocurrencies see some use
* Platforms **perceived as private** are popular
  
  (Whatsapp, Discord, Telegram, Slack)
* VPNs, Tor getting more mainstream
