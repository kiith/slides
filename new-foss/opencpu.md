* OpenPOWER
  - TALOS - workstations open from firmware up

* RISC-V
  - Possible 'linux of CPUs?'
  - Open instruction set that anyone can add to
  - Adopted by Western Digital, research projects (4096-core, CPU-with-NN, etc.)
  - Devboards exist

