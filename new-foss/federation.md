* Before Facebook and such "products" we had **protocols** - mail, IRC.
* Servers interoperating through a protocol - **federated** service
* No corporation needed, no design for user exploitation
* Forkable code, **interoperating** services
* Diverse: Pleroma has a **gopher** frontend! Mathstodon has **LaTeX** support!

* **Mastodon** / **Pleroma** social - **0.5-1.5M users** and growing
* **Matrix** chat: why are there OSS projects using Slack/Discord?
* **Pixelfed** images, **Peertube** videos, **Write.as** blogs ...
* Near future: Git hosting, libraries? journals? reddit?
* **ActivityPub** - W3C protocol for federated social networking


- Also **ssb** / **patchwork**, **IPFS**, **Dat**...
- **TODO**: v01d mastodon/pleroma instance.
- **University servers?** - Mastodon is cool *now*.



