- Snap/Flatpak
- PS3 emulation
- Neuralink, voting vulnerabilities and such: OSS more important than ever!
- Mini computers
- Proton
- GDPR
- Linus getting a sabbatical
- Redox, Haiku, Fuchsia
- KDE is faster than GNOME
- New C-like languages getting a foothold
- Librem, postmarketos, More hardcore stuff, no longer trying to beat Android
