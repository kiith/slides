* `callgrind`/`cachegrind` + `kcachegrind`:
  - valgrind's profilers + a (very good) GUI for them.
  - profiles your code in Valgrind's VM - *very* distorted performance.
  - gets *exact* instruction counts, which may be useful

* `massif`:
  - like `heaptrack`, but slower

* `gdb TUI`, `GDB-dashboard`
  - do not underestimate GDB

* `strace`/`ltrace`:
  - Find out what that program is doing
  - If you don't know these, google them, *extremely* useful

* `apitrace`:
  - Record all your OpenGL calls and replay them
    And see the frame as it's getting built call by call

* `QT Creator's perf frontend`, `Intel VTune`, `AMD uProf`
