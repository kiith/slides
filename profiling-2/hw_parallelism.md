* Memory bandwidth is limited.
* One memory location *cannot be written simultaneously* (reading is OK)
* **Write sharing** - many cores writing single location

 If your parallel, *even lock-free*, algorithm writes a
 *single counter/pointer/etc*, you're *sequentially limited*

 Kills parallel algorithms with a single epoch/state/whatever counter

* **False sharing**; writing to same *cache line*. E.g:

struct A {
    int written_by_one_thread;
    int written_by_another_thread;
}

Use **padding** to prevent sharing (usually 64B)

struct B {
    int written_by_one_thread;
    ubyte[CACHE_LINE_SIZE] cache_padding;
    int written_by_another_thread;
}
