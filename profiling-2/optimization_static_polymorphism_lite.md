

#1: Dynamic polymorphism with virtuals

```
class VideoDevice {};
class OpenGL2VideoDevice: public VideoDevice {};
class VulkanVideoDevice: public VideoDevice {};
```

* **Good**: Readable, can change implementations at runtime
* Bad, **but not really**: Double indirection, but not really
* **Bad**: Mostly un-inlinable (LTO may help, sometimes)

#2: Dynamic polymorphism with function pointers

```
class VideoDevice
{
    void (*draw)();
};

VideoDevice build_opengl2_video_device()
{
    VideoDevice device;
    device.draw = &gl_draw;
}
```

* **Good**: Single instead of double indirection. Useless on x86
* **Bad**: Harder to maintain
* **Bad**: Function pointers in all instances

#3: Static polymorphism by typedef

```
class OpenGL2VideoDevice {};
class VulkanVideoDevice {};

using VideoDevice = OpenGL2VideoDevice;
```

* **Good**: Inlinable
* **Good**: Easy to write
* **Bad**: Need to check that implementations do not get out of sync
* **Bad**: Can only choose implementation at compile-time

#4: Static polymorphism by template parameter
```
template<VideoDevice>
void code_using_video_device()
{
    VideoDevice device();
    ...
}

...

if( video_device == "opengl" )
{
    code_using_video_device<OpenGL2VideoDevice>()
}
else if( video_device == "opengl" )
{
    code_using_video_device<VulkanVideoDevice>()
}

```

* **Good**: Inlinable
* **Good**: Can change implementation at runtime
* **Bad**: Code bloat
* **Bad**: C++ template syntax
