* Most modern CPUs are pipelined

  Multiple instructions processed simultaneously

* Need to **predict** which path a *branch* will take
* Branch prediction fail example;

     // char data[];
     for (int c = 0; c < size; ++c) {
         if (data[c] >= 128) sum += data[c];
     }


  Sorted data: `2.352s` Random data: **11.777s**

* Cache vs branches (Compiled FSM vs table FSM example)

Optimizations ( **profile** before doing these):
- Branch-less code (e.g. calculate both, bit-mask the result)
- Branch prediction hints (GCC `__builtin_expect`) (not that useful)
- By default, the `if()` branch is considered more likely

