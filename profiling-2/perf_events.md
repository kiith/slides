* instructions:                Instructions executed
* **cycles**:                  CPU cycles. **IPC**: instructions/cycles
* branches:                    All branches
* branch-misses:               Mispredicted branches (~5ns)
* L1-Xcache-XXXX-misses:       L1 cache misses (<=7ns)
* cache-references:            Lmax cache accesses (~7ns)
* **cache-misses**:            Lmax cache misses (~100ns)
* faults:                      Page faults (non-compact memory)
