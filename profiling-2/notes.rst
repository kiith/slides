"LuaJIT example":

LuaJIT is a much faster implementation of Lua, but does not support all features of modern Lua, so those need to be avoided.
Also, LuaJIT has an FFI allowing using C types / functions in Lua with no extra overhead - but the opposite is *much* slower.

So while the obvious thing would be to have a C program calling Lua where needed,
it is often much more efficient to have a Lua program that calls C functions.


"Compiled FSM vs table FSM example"

Intuitively, compiling a FSM to directly go to next state (removing indirection)
instead of 'interpreting' it with a state transition table would help performance -
but in this case we're just moving work from the data cache (table) to branch
predictor + instruction cache (generated code); With the table, our code is short
and easy predict *and* our states are likely already in data cache, while with
a compiled FSM, there is less data cache work, but the branch predictor is 
*overwhelmed* and unable to predict our huge, branching code, resulting in
massive slowdown.
already
