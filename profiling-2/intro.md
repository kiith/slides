**Your program is slow.**

* It is not going to get faster by itself
* No more free lunch/Megahertz
* Moore's "law" is dead
* If you're using Electron, you're **murdering the planet**.

**Some things may only use one core.**

* But you *can* use the crap out of a single core for a 10x/1000x speedup

  ~10x: use C/++/Rust/D over Python/JS
  ~10x: remove bloated dependencies
  ~10x: code for the HW (cache, SIMD, NUMA layout, IO kernel bypass ...)

Some problems are **not distributable**.

* Communication may be too expensive, *latency* unacceptable
* Need 90FPS+ **in VR** to avoid puking, no stutter
* Games in general
