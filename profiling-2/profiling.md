Profile **first**, optimize *later*.

- Otherwise you **will** optimize code that's **not slow**.
- This includes algorithm choice.
- A slow implementation is OK **if it can be rewritten**

Linux has a number of awesome profiling/debugging tools for this.

**perf top/record/stat**, valgrind/callgrind/massif, kcachegrind,
**heaptrack**, **hotspot**, ltrace/strace, apitrace, **eBPF**, 
etc.

And of course, you can write your own tools.
