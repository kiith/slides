**Tools:**
 * perf examples: `http://www.brendangregg.com/perf.html`
 * heaptrack: `https://github.com/KDE/heaptrack`
 * hotspot: `https://github.com/KDAB/hotspot`
 * mozilla rr: `http://rr-project.org/`
 * GDB-dashboard: `https://github.com/cyrus-and/gdb-dashboard`
Agner Fog:
 * `http://www.agner.org/optimize/` (Optimizing software in C++ in particular)
**Ulrich Drepper**: *What every programmer should know about memory*
 * `https://lwn.net/Articles/250967/`
 * `https://akkadia.org/drepper`
**Paul E. McKenney**: *Is Parallel Programmin Hard, And, If So,...*
 * `https:://arxiv.org/abs/1701.00854`
Intel/AMD: (the Optimization Guides in particular)
 * `https://software.intel.com/en-us/articles/intel-sdm#optimization`
 * `https://developer.amd.com/resources/developer-guides-manuals/`
 * ARM: Google: Cortex $VERSION Software Optimization Guide

   E.g. **Cortex A72 Software Optimization Guide**


