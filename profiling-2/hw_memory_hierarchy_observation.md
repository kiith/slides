We (probably) live in a physical world.

**Observation**

Speed of light is a thing --> no such thing as **O(N)**.

Greater N --> more memory --> **greater** physical volume -->
**more physical distance** between your components
--> ( **cuberoot(N)** overhead per item)
