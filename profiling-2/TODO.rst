- more stuff from previous profiling talk
- stuff from the ARM OSSVikend talk?
- print out code samples (like in hw_parallelism)
  with a c highlighter
- easy optimizations (compiled languages):

  - Static polymorphism lite - split the file into 4 subfiles and show it!
  - freelists! / avoiding allocations!
  - slices instead of copies (esp. when e.g. parsing!)
  - C++ STL containers are slow!
  - return by value instead of reference
  - arrays instead of anything else
  - loop unrolling
  - branch predictability - first branch is taken


**low-priority**

* ASCII art charts for stuff
  - 'moore's law is dead' - the decline of CPU speedups
  - write sharing: an image of multiple cores trying
    to write to the same location
* GL/ASCII/Processing demo-like animations for stuff like the 'no O(N)'
  observation in the memory hierarchy slide
* allow restarting the session from 'pause points.' ('press Enter')
  so we don't fail by getting stuck half-way
