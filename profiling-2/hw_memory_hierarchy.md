* CPUs are fast. RAM is slow and **n * 10cm** away!
* Need a fast, close cache.

  - Cache is fast, RAM is slow, far away

    * Need L2 cache

      - ... L3 cache
      - ... Core clusters - distant caches
      - ... NUMA nodes - distant memory
      - ... Fast NVM (3D XPoint, etc.), SSD swap ...

Optimizations:
- Few large > many small allocations. Arrays > lists/trees.
- Sequential data processing
- Keep data small *and compact* (memory bandwidth)
- Keep code small (`-Os`)
