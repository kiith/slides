Flags
* `-O2`: Usual 'optimized build'.
* `-O3`: May or may not be better than O2 (code bloat, icache)
* `-g`: Minimum overhead, needed for practical profiling
* `-fno-inline`: Readable profiler output, breaks *a lot* of optimizations
* `-fno-omit-frame-pointer`: Needed to get readable callgraph info from `perf`

Do not forget to set/unset `NDEBUG`.

Language extensions:
* `__attribute__ ((noinline))`, `__attribute__((always_inline))`
* `__attribute__ ((pure))`
* `__builtin_expect`

Rules of thumb:
* Do not profile in debug mode
* `-O2 -fno-inline`: get an *overview of what's eating the CPU*
* `-O2`: **exact** results, and for final micro-optimizations.
* `-O3` instead of `-O2` if it's faster.
