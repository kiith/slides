**Low-level (mostly C++)**

* **Bad**: **allocating resources** in private code of a class
* **Good**: Memory/internals allocated and **passed by the user**

  Use `unique_ptr`, moving

  In simple programs allocations can moved up to `main()`

* **Bad**: do-it-all interface when we need a few features
* **Good**: **YAGNI** - minimal interface with no more features than needed *now*.

  Smaller interfaces are easier to implement *and* optimize.

* **Bad**: **std::function** in API prevents inlining forever.
* **Bad**: with virtuals calling virtuals - hard to profile.
* **Good**: function-by-template allows any kind of function, inlined if possible.

* **Bad**: Class with many instances allocated/managed by user.
* **Good**: Use such objects through a container-like 'manager' with handles

  Bonus points if user passes the memory (resources)
