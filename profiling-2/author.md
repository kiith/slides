* Ferdinand Majerech
* V01D hacKErspace, ESET
  `https://v01d.sk` , `https://gitlab.com/v01d.sk`
* C/C++/D/ARM/x86/BPF/Python/etc OSS dev
* `kiith@gitlab`, **formerly** `kiith-sa@github`
* `enum@tutanota.com`

Slides: `https://gitlab.com/kiith/slides`
