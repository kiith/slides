**Project-level**

* **Bad**: Wrong tool for the job
* **Good**: High-level != VM (D,Rust,Go,Swift...)

  Electron/JVM/etc won't ever have VR latency / fit into $BOARD's RAM.
  Code quality does not depend on the number of cool new features you use.

* **Bad**: "We don't have time to profile" *until too late*
* **Good**: Quick look at `perf` after a change; you *do have* **3 minutes**
* **Good**: `perf stat` in CI, warn on unexpected slowdowns

* **Bad**: **Un-isolated** heavy-weight dependencies
* **Good**: Wrap dependencies. *Use what you need, no more.*
* **Good**: Copy/rewrite one function instead of pulling an entire framework.

  Publish a library if your rewrite is good **and longer than 5 lines**

* **Bad**: *Death by abstraction* (too many layers -> difficult profiling)
* **Good**: If it results in simpler code, write the implementation directly.
