#!/bin/bash


TIME_HTOP=30

# USAGE:
# See the presentation:  tmux attach -t TALK
# Kill the presentation: tmux kill-session -t TALK

START_SECTION=${1:-defaultValue}
REACHED_START=1
SESSION=TALK
TIMEOUT="timeout --foreground"

check_start_section () {
	if [ "$START_SECTION" == "$1" ]; then
		REACHED_START=0
	fi
	return $REACHED_START
}

# shows the first argument on the 'message pane'
show_big () {
    tmux select-pane -t 1
    tmux resize-pane -y 6
    tmux send-keys C-u "figlet -c -t -f small " "$1" " " C-m
    tmux select-pane -t 0
}

show () {
    tmux select-pane -t 1
    tmux resize-pane -y 2
    tmux send-keys C-u "        <<<<<< " "$1" " >>>>>>"
    tmux select-pane -t 0
}

shopt -s expand_aliases
alias type_keys='tmux send-keys'


section_intro () {
    check_start_section "intro" || return
    show_big "author"
    type_keys "mdless --no-pager author.md" C-m
    read -p "Enter to continue"

    show_big "intro"
    type_keys "mdless --no-pager intro.md" C-m
    read -p "Enter to continue"
}

section_pessimization () {
	check_start_section "pessimization" || return
    show_big "Premature pessimization"
    type_keys "mdless --no-pager premature_pessimization.md" C-m
    read -p "Enter to continue"

    show_big "Premature pessimization - project"
    type_keys "mdless --no-pager premature_pessimization_project.md" C-m
    read -p "Enter to continue"
    show_big "Premature pessimization - code"
    type_keys "mdless --no-pager premature_pessimization_code.md" C-m
    read -p "Enter to continue"
}
section_hw () {
	check_start_section "hw" || return
    show_big "HW: memory hierarchy"
    type_keys "mdless --no-pager hw_memory_hierarchy.md" C-m
    read -p "Enter to continue"
    type_keys "mdless --no-pager hw_memory_hierarchy_observation.md" C-m
    read -p "Enter to continue"

    show_big "HW: branches"
    type_keys "mdless --no-pager hw_branches.md" C-m
    read -p "Enter to continue"

    show_big "HW: parallelism"
    type_keys "mdless --no-pager hw_parallelism.md" C-m
    read -p "Enter to continue"

    show_big "HW: I/O"
    type_keys "echo TODO / HDD / SSD / network / file reading" C-m
    read -p "Enter to continue"

    # LATENCY NUMBERS {
    # TODO eventually turn this into an ASCII-art chart - with ncurses or
    # something, and may even measure some of the numbers on the current PC
    show_big "Latency numbers"
    type_keys "gimp latency-big.png" C-m
    read -p "Enter to continue"
    # } LATENCY NUMBERS
}

section_compiler () {
    check_start_section "compiler" || return
    show_big "Compiler features"
    type_keys "mdless --no-pager compiler.md" C-m
    read -p "Enter to continue"
}

section_profiling () {
    check_start_section "profiling" || return
    show_big "profiling"
    type_keys "mdless --no-pager profiling.md" C-m
    read -p "Enter to continue"

    show_big "gprof"
    type_keys "figlet -c -t -f big 'Do not use gprof.' " C-m
    type_keys "figlet -c -t -f small 'except for call counting.' " C-m
    read -p "Enter to continue"
}


section_hotspot_perf () {
     check_start_section "hotspot" || return
     show_big "hotspot & perf"
     sleep 2
     # HOTSPOT {
     type_keys "mdless --no-pager perf_hotspot.md" C-m
     read -p "Enter to continue"
     # candidates:
     #     - handbrake
     #     - blender
     #     - parallel archiving
     #     - some mate app
     #     - some emulator (look at retroarch command)
     #     - hotspot itself
     #     - gimp
     #     - firefox?
     #     - htop?
     type_keys "hotspot"
     sleep 2
     type_keys C-m
	 show "DEMO-record,events,flamegraph,filter-threads/interval,callgraph"
     read -p "Enter to continue"
     type_keys q C-m
     # } HOTSPOT
     show_big "perf"
     sleep 1
     # PERF LIST {
     show "perf list to get measurable events"
     type_keys "perf list"
     sleep 2
     type_keys C-m
     read -p "Enter to continue"
     type_keys q C-m
     type_keys "mdless --no-pager perf_events.md" C-m
     read -p "Enter to continue"
     # } PERF LIST
     # PERF STAT {
     show_big "perf stat"
     sleep 3
     show "count events, great for benchmarking!"
     type_keys "perf stat blender"
     sleep 2
     type_keys C-m
     read -p "Enter to continue"
     show "detailed with more events (multiplexing, less precise)"
     type_keys "perf stat -ddd blender"
     sleep 2
     type_keys C-m
     read -p "Enter to continue"
     # } PERF STAT
     # PERF TOP {
     show_big "perf top: top, but with functions "
     type_keys "perf top"
     sleep 3
     show "can drill down to assembly level"
     type_keys C-m C-m
     read -p "Enter to continue"
     type_keys q
     sleep 1
     type_keys q C-m
     sleep 1
     show "measure system-wide cycles, cache misses, page faults"
     type_keys "perf top -ecycles,cache-misses,faults"
     sleep 4
     type_keys C-m C-m
     sleep 1
     type_keys C-m
     show "<Tab> to switch between measured events"
     read -p "Enter to continue"
     type_keys q
     type_keys q C-m
     show ""
     # } PERF TOP
     # PERF RECORD/REPORT {
	 show_big "perf record"
     show "-F: frequency, -e: events, -g: call graph info"
	 # note: this is what hotspot uses
     type_keys "perf record -g -e cycles,instructions,cache-misses -F 5000 nvim talk.sh"
     sleep 2
     type_keys C-m C-m
     sleep 7
#     # NOTES:
#     # - switch events
#     # - search
#     # - sort
#     # - drill down to asm
#     # - show jumps
#     # - jump between most expensive ops
     type_keys "perf report"
     sleep 2
     type_keys C-m C-m
     show "<DEMO (asm view, jumps, event switching)>"
     read -p "Enter to continue"
     type_keys q C-m
     show "Can also be opened with hotspot"
     sleep 2
     type_keys "hotspot perf.data"
     read -p "Enter to continue"
     type_keys C-m C-m
     read -p "Enter to continue"
     type_keys q C-m
     # } PERF RECORD/REPORT
}

section_time () {
    check_start_section "time" || return
    show_big "time"
    type_keys "/usr/bin/time ./cfg-debug test.cfg 1000" C-m
    read -p "Enter to continue"
    show "with -v this gets much more interesting"
    type_keys "/usr/bin/time -v ./cfg-debug test.cfg 1000" C-m
    read -p "Enter to continue"
}

section_heaptrack () {
    check_start_section "heaptrack" || return
    show_big "heaptrack"
    type_keys "mdless --no-pager heaptrack.md" C-m
    read -p "Enter to continue"
    type_keys "heaptrack blender"
    sleep 2
    type_keys C-m
    read -p "Enter to continue"
    type_keys ZZ C-m
    type_keys 'heaptrack_gui heaptrack.*.gz'
    sleep 2
    type_keys C-m
    show_big "<DEMO>"
    read -p "Enter to continue"
    killall heaptrack_gui
}

# section_hotspot () {
#     check_start_section "hotspot" || return
#     show_big "hotspot"
#     type_keys "mdless --no-pager hotspot.md" C-m
#     read -p "Enter to continue"
#     type_keys "perf record -ecycles,cache-misses -F5000 -g ./cfg-profile test.cfg 10000" C-m C-m
#     sleep 10
#     type_keys "hotspot"
#     sleep 2
#     type_keys C-m
#     show_big "<DEMO>"
#     read -p "Enter to continue"
# }

section_rr () {
    check_start_section "rr" || return
    show_big "RR"
    type_keys "mdless --no-pager rr.md" C-m
    read -p "Enter to continue"
}

section_valgrind () {
    check_start_section "valgrind" || return
    show_big "valgrind: memcheck"
    # TODO indeed
    type_keys "echo TODO" C-m
    read -p "Enter to continue"
}

section_bpf () {
    check_start_section "bpf" || return
    show_big "BPF / eBPF"
    # TODO indeed
    type_keys "echo TODO/verification/bcc" C-m
    read -p "Enter to continue"
}

section_mentions () {
	check_start_section "mentions" || return
    show_big "honorable mentions"
    type_keys "mdless --no-pager mentions.md" C-m
    read -p "Enter to continue"
}

section_outro () {
	check_start_section "outro" || return
    show_big "resources"
    type_keys "mdless --no-pager resources.md" C-m
    read -p "Enter to continue"

    show_big "v01d"
    type_keys "mdless --no-pager v01d.md" C-m
    read -p "Enter to continue"
}

# TODO GDB config

# Start the actual code.

for (( i = 200; i > 0; i-- )); do
    echo ""
done
tmux kill-session -t $SESSION
tmux -2 new-session -d -s $SESSION
# tmux -2 attach-session -t $SESSION &
tmux split-window -v
tmux select-pane -t 1 -P 'bg=white'
tmux select-pane -t 1 -P 'fg=black'
tmux resize-pane -y 5
# type_keys "tmux resize-pane -D 5" C-m
tmux select-pane -t 0
# type_keys "cd ~" C-m

for (( i = 10; i > 0; i-- )); do
    show_big "$i"
    sleep 1
done


section_intro
section_pessimization
section_hw
section_profiling
section_hotspot_perf
#section_perf
section_compiler
section_time
section_heaptrack
#section_hotspot
section_rr
section_valgrind
section_bpf
section_mentions
section_outro

show_big "The End / Questions"
read -p "Enter to continue"
tmux kill-session -t $SESSION
