#!/bin/sh
g++ cfg.cpp -std=c++17 -g -fno-omit-frame-pointer -o cfg-debug
g++ cfg.cpp -std=c++17 -g -O3 -fno-inline -fno-omit-frame-pointer -o cfg-profile
g++ cfg.cpp -std=c++17 -g -O3 -fno-omit-frame-pointer -o cfg-release
