**Premature pessimization - the root of much evil**

* *Pessimization*: decisions making code **hard to optimize**
* High level != un-optimizable
* Do not ignore performance in the design phase ('can be done later')
* Do not save 5 hours of developer time by spending 4 years of user time
* Avoid **cutting yourself off** from good performance
