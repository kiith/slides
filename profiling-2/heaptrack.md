Like Valgrind's Memcheck+Massif, **but faster**

* And has a nice QT-based UI
  - Allocations / allocated memory by time
  - Allocations / allocated memory caller/callee graph
  - Allocation size histogram
  - Leaks
* By Milian Wolff (of Massif Visualizer, KDevelop, Hotspot fame)
* Does not slow down your program horribly
