#!/bin/bash


TIME_HTOP=30


SESSION=LIGHTNING
TIMEOUT="timeout --foreground"


show () {
    tmux select-pane -t 1
    tmux resize-pane -y 2
    tmux send-keys C-u "<<<<<< " "$1" " >>>>>>"
    tmux select-pane -t 0
}

# Modern Unix Commands

tmux kill-session -t $SESSION
tmux -2 new-session -d -s $SESSION
# tmux -2 attach-session -t $SESSION &
tmux split-window -v
tmux select-pane -t 1 -P 'bg=white'
tmux select-pane -t 1 -P 'fg=black'
tmux resize-pane -y 5
# tmux send-keys "tmux resize-pane -D 5" C-m
tmux select-pane -t 0
tmux send-keys "cd ~" C-m

show "10"
sleep 3
show "7"
sleep 2
show "5"
sleep 1
show "4"
sleep 1
show "3"
sleep 1
show "2"
sleep 1
show "1"
sleep 1

# TODO later break this up into subscripts, and refactor sleeping+doing things
#      (doing things after a delay)

# INTRO {
show "Ferdinand Majerech"
sleep 5
show "V01D hackerspace: https://v01d.sk, ESET"
sleep 7
show "C/C++/D/etc development"
sleep 7
show "Open source dev, kiith@gitlab , kiith-sa@github"
sleep 10
# } INTRO


# HTOP {
show "htop [top]"
sleep 7
tmux send-keys "htop" C-m
sleep 10
show "get help (h)"
tmux send-keys "h" #on
sleep 5
tmux send-keys "h" #off
sleep 5

show "show kernel threads (Shift-k)"
tmux send-keys "K" #on
sleep 5
tmux send-keys "K" #off
show ""
sleep 3

show "show process threads (Shift-h)"
tmux send-keys "H" #on
sleep 5
tmux send-keys "H" #off
show ""
sleep 3

show "sort by memory (Shift-m)"
tmux send-keys "M"
sleep 3
show "sort by CPU (Shift-p)"
tmux send-keys "P"
show ""
sleep 3

show "kill processes (F9)"
tmux send-keys F9
sleep 5
tmux send-keys Escape
show ""
sleep 3

show "set Cores to use by a process (a)"
tmux send-keys a
sleep 5
tmux send-keys Escape
show ""
sleep 3

show "show files open by process (l)"
tmux send-keys l
sleep 7
tmux send-keys Escape
show ""
sleep 3

show "configure (F2)"
tmux send-keys F2
sleep 2
show "Display options!"
tmux send-keys Down
sleep 3
tmux send-keys Down
sleep 2
show "More data! Kernel/User time! Page faults!"
tmux send-keys Down
sleep 3
show "add load info!"
tmux send-keys Up
tmux send-keys Up
tmux send-keys Up
sleep 4
tmux send-keys Right
sleep 1
tmux send-keys Right
sleep 1
tmux send-keys Right
sleep 1
tmux send-keys Down
sleep 1
tmux send-keys Down
sleep 1
tmux send-keys Enter
sleep 1
tmux send-keys Left
sleep 1
tmux send-keys Enter
sleep 1
tmux send-keys Escape
sleep 5
tmux send-keys Escape
show ""
sleep 3

tmux send-keys "q"
# } HTOP


# FUCK {
show "fuck [N/A] (sudo pip3 install thefuck)"
sleep 7
tmux send-keys "aptget install" C-m
sleep 5
show "corrects typo"
tmux send-keys "fuck" C-m
sleep 5
tmux send-keys C-m
sleep 5
show "corrects missing sudo"
tmux send-keys "fuck" C-m
sleep 5
tmux send-keys C-c #exit instead of installing something during talk
sleep 5

# } FUCK

# APT {
show "apt [apt-get] (debian/ubuntu/mint/etc)"
sleep 7
show "4 less keypresses, and has a progress bar!"
tmux send-keys "sudo apt update"
sleep 15
show "4 less keypresses, and has a progress bar!"
tmux send-keys C-u "sudo apt install pkg"
sleep 15
show "4 less keypresses, and has a progress bar!"
tmux send-keys C-u "sudo apt full-upgrade"
sleep 15
show "list all packages with the same tool!"
tmux send-keys C-u "apt list"
sleep 3
tmux send-keys C-m
sleep 10
show "list all installed packages"
tmux send-keys C-u "apt list --installed"
sleep 3
tmux send-keys C-m
sleep 5
show "list packages with globs"
tmux send-keys C-u "apt list '*grind'"
sleep 3
tmux send-keys C-m
sleep 5
# } APT

# TMUX {
show "tmux [screen]"
sleep 7
show "used to create this talk"
sleep 10
show "'tiling window manager' over SSH"
sleep 10
show "persistent 'tiled desktops', connect to them at any time"
sleep 10
show "infinitely configurable"
sleep 5
show "actively maintained, unlike screen"
sleep 5
show "actively being improved, unlike screen"
sleep 5
show "etcetcetc"
sleep 5
# } TMUX


# NCDU {
show "ncdu [du] (ncurses du)"
sleep 7
tmux send-keys "ncdu" C-m
sleep 15
tmux send-keys Down
sleep 1
tmux send-keys C-m
sleep 1
tmux send-keys Down
sleep 1
tmux send-keys C-m
sleep 1
tmux send-keys Down
sleep 1
tmux send-keys C-m
sleep 1
tmux send-keys Down
sleep 1
tmux send-keys C-m
show "yay my games"
sleep 15
show "move around,  press 'd' to delete files"
sleep 15
tmux send-keys q
show ""
# } NCDU


# EXA {
show "exa [ls] (the.exa.website)"
sleep 7
show "better ls"
tmux send-keys "exa"
sleep 2
tmux send-keys C-m
sleep 5
show "the colors! (esp. the permissions)"
tmux send-keys "exa -l"
sleep 2
tmux send-keys C-m
sleep 15
tmux send-keys "cd slides"
sleep 1
tmux send-keys C-m
sleep 2
show "a readable recursive tree"
tmux send-keys "exa -lT" C-m
sleep 10
tmux send-keys "exa -lT --git" C-m
show "see git status of files"
sleep 15
tmux send-keys "cd .." C-m
sleep 2
show ""
# } EXA

# PANDOC {
show "pandoc [N/A] (convert any document to any document)"
sleep 7
show "pandoc (used in darktimers)"
sleep 5
show "pandoc (input formats)"
tmux send-keys "pandoc --help | head -6" C-m
sleep 15
show "pandoc (output formats)"
tmux send-keys "pandoc --help | head -13 | tail -7" C-m
sleep 15
show "pandoc markdown - markdown enhanced for papers"
sleep 5
show "options for tweaking HTML, PDF (LaTeX) output"
sleep 5
show "examples w/ ReStructuredText input"
tmux send-keys "cd slides/vim-workshop-1" C-m
sleep 2
tmux send-keys "exa -l" C-m
sleep 5
show "simple HTML example (without CSS)"
tmux send-keys "pandoc README.rst -o README.html" C-m
sleep 5
tmux send-keys "midori README.html" C-m
sleep 10
killall midori
sleep 5
show "simple PDF example (requires texlive-full)"
tmux send-keys "pandoc README.rst -o README.pdf" C-m
sleep 5
tmux send-keys "evince README.pdf" C-m
sleep 10
killall evince
sleep 5
show "ODT example"
tmux send-keys "pandoc README.rst -o README.odt" C-m
sleep 5
tmux send-keys "abiword README.odt" C-m
sleep 10
killall abiword
sleep 5
show "DOCX example (YMMV)"
tmux send-keys "pandoc README.rst -o README.docx" C-m
sleep 5
tmux send-keys "abiword README.docx" C-m
sleep 10
killall abiword
sleep 5
tmux send-keys "rm -f README.html README.pdf README.odt README.docx" C-m
tmux send-keys "cd ../../" C-m
show ""
# } PANDOC

# PERF FAMILY {
show "perf family (linux-tools-common)"
sleep 5
# PERF TOP {
show "perf top [top]"
sleep 7
show "like top, but see overhead by functions"
tmux send-keys "perf top" C-m C-m
sleep 15
show "can drill down to assembly level"
tmux send-keys "/malloc" C-m
sleep 2
tmux send-keys C-m
sleep 1
tmux send-keys C-m
sleep 1
tmux send-keys C-m
sleep 15
tmux send-keys q
sleep 1
tmux send-keys q
sleep 1
show "use perf list to get measurable events"
tmux send-keys "perf list" C-m
sleep 20
tmux send-keys q
show "measure system-wide stalled cycles, cache misses, page faults"
tmux send-keys "perf top -ecycles,stalled-cycles-frontend,cache-misses,faults" C-m C-m
sleep 10
tmux send-keys C-m
sleep 10
show "Tab to switch between measured events"
tmux send-keys Tab
sleep 5
tmux send-keys Tab
sleep 5
tmux send-keys Tab
sleep 10
tmux send-keys Tab
sleep 5
tmux send-keys q
show ""
# } PERF TOP
# PERF STAT {
show "perf stat [N/A]"
sleep 7
show "basic overview"
tmux send-keys "perf stat gzip -1kf bench1.webm" C-m C-m
sleep 15
show "detailed with more events (multiplexing, less precise)"
tmux send-keys "perf stat -ddd gzip -1kf bench1.webm" C-m C-m
sleep 20
show "specific events"
tmux send-keys "perf stat -ealignment-faults,emulation-faults gzip -1kf bench1.webm" C-m C-m
show ""
sleep 12
# } PERF STAT

# } PERF FAMILY

# DD status=progress {
show "dd status=progress option"
sleep 8
show "(traditional tool for block copies)"
sleep 7
show "see the progress. no more wondering if 1 hour or 1 day"
tmux send-keys "dd if=bench1.webm of=bench2.webm status=progress bs=16 " C-m
sleep 20
show ""
# } DD status=progress

show "The end"
tmux kill-session -t $SESSION
