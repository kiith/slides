============================
Vim - extending the language
============================


.. TODO vim for people who think things like vim are hard
.. XXX end: some cool vim video?

.. XXX plugin screenshots/gifs (often on plugin pages)


------------
Introduction
------------

https://gitlab.com/kiith/slides/tree/master/vim-workshop-2

I'm Ferdinand Majerech

Member of the **v01d** hackerspace: https://void.sk

Software engineer at ESET (network security)

Open source dev (https://gitlab.com/kiith, formerly https://github.com/kiith-sa)

C/C++/D/low-level-ish Linux dev



--------------------------
Previously on Vim workshop
--------------------------

https://gitlab.com/kiith/slides/tree/master/vim-workshop-1


--------
Mappings
--------

*When I press this key, I want you to do this stuff instead of whatever you
would normally do.* (from `Learn Vimscript the Hard Way
<http://learnvimscriptthehardway.stevelosh.com/chapters/03.html>`_)

* ``:noremap - dd<Enter>``: ``-`` does what ```dd`` does (**try it**)
* ``:unmap - <Enter>``: remove a mapping

Try this; any mappings will be forgotten on shutdown.
Add them to ``.vimrc``/``.config/nvim/init.vim`` to make them permanent.

Plain ``:noremap`` is **too general**; works in *multiple modes* (though not all of them)

* ``:nnoremap`` Map in **normal** mode: ``nnoremap ck i<CR><Esc>k$`` will make ``c`` followed by ``k``
  break the current line at current position.

  In Vim, we can map **key sequences**. This is much more
  hand-friendly than Escape Meta Alt Control Shift.
* ``:vnoremap`` Map in **visual** mode: ``vnoremap > >gv`` will make ``>`` reselect
  current selection after indenting
* ``:inoremap`` Map in **insert** mode: ``inoremap <C-t> <Esc>viw~`` (note the
  ``<Esc>`` and ``a``) will toggle case of the current word in insert mode.
* ``:onoremap`` Map in **operator pending** mode (see further down)
* and many more (``cnoremap``, ``lnoremap`` ... see ``:help map``)

.. note:: Why ``noremap`` instead of ``map``?

    ``:map``, ``:nmap``, etc. are **recursive**. Try:
    #. ``:map n apasta<Esc>``
    #. ``:map m nnn``
    #. Press ``m`` in normal mode
    #. (use ``:unmap`` to disable the mappings)

    Recursive mappings are useful for creating complex commands, *mapping plugin features*
    and **brutally destroying your files with unintended side effects 2 years later
    when you change your config**. That's why.

**Q: Where are the free keys?**

.. image:: keyboardmap.jpg
   :width: 800px

**A: Leader, key sequences**

Leader is just a name for the key you most commonly use to start mapping
sequences.  Specifying leader makes it easy to remap this prefix later, but
you could also just map the key directly.

``:let mapleader = ","``

``:nnoremap <leader>k i<CR><Esc>k$`` is same as mapping ``,k``
 
-------------------
Examples - mappings
-------------------

**What do these do?** Why are they useful? What are their problems? Try them out!

Hint:

.. image:: keyboardmap.png


#. ``nnoremap H ^``, ``nnoremap L $``
#. ``nnoremap <Left> <nop>``, ``:nnoremap <Right> <nop>``, ``:nnoremap <Up> <nop>``, ``:nnoremap <Down> <nop>``
#. ``inoremap jk <Esc>``, ``inoremap <Esc> <nop>``
#. ``nnoremap ' :``
#. ``nnoremap <leader>v :vsp $MYVIMRC<cr>``
#. ``nnoremap <leader>: :%s:::cg<Left><Left><Left><Left>``
#. ``nnoremap Q vipgq<Esc><esc>``
#. ``nnoremap <leader>' viw<esc>a'<esc>bi'<esc>lel``
#. ``nnoremap <leader>; :%s:\<<C-r>=expand("<cword>")<CR>\>::gc<Left><Left><Left>``

* When learning a better mapping, it is useful to disable the
  original with a ``<nop>`` mapping.

* If you find these useful, add them to your
  ``.vimrc``/``.config/nvim/init.vim``.

* Look at ``:help H`` to see what commands mapping ``H`` overwrites. Useful to
  identify "mappable" keys.

-------------------
Exercise - mappings
-------------------

**Use a file you can afford to destroy**

Create the simplest mapping to achieve the following:

#. Normal mode: Make the current word bold in Markdown ( ``word`` -> ``**word**`` )

#. Normal mode: move current line upwards/downwards.

#. Visual mode: Make the currently selected text bold (``a long sentence`` ->
   ``**a long sentence**``)

#. Command mode: ``<C-r>"`` will paste currently yanked text into the command line.
   This makes it easy to test configuration chages.

   Create a mapping to do this faster.

#. Normal mode: Turn the current line into a Marodown heading:

   ``heading`` ->

   .. code::

      =======
      heading
      =======

#. What does ``:nmap dd O<esc>jddk`` do?


-------------------------
Operator pending mappings
-------------------------

``:omap``/``:onoremap``

**Operator pending** mode: The 'mode' between a verb (operator) and a noun
(movement).

Example: ``di"`` (delete in double-quotes):

#. ``d`` enters operator pending mode.
#. ``i"`` is processed *in operator pending* mode and finishes the command

Now try:

``:onoremap q i"``

Entering ``dq`` will now execute ``di"`` ("delete quotes").
Similar for ``cq``, etc.

This allows you to define **your own nouns/movements**.

It is possible to define nouns like *markdown heading of this
section*, *doxygen description for parameter under cursor* or *e-mail address of the
current mail** though it requires a bit more work.

.. TODO in the next version of this workshop, show how to implement the above
        (learn the Hard Way)


------------------------------------
Exercise - operator pending mappings
------------------------------------

What does this do?

``:onoremap b /*\/<Enter>``

.. the above is a movement 'till the end of a C comment'

Try to come up with ways to use operator mappings - see
https://gitlab.com/kiith/slides/tree/master/vim-workshop-1
for reference.


------
Macros
------

TODO

.. TODO editing macros,
.. TODO applying macro to entire file,
.. TODO to many files
.. TODO show an example of using a mapping to simplify this


-----------
Vim plugins
-----------

#. **Vim language extensions** << today
#. UI/Cosmetic
#. Language support (syntax, etc.)
#. Tool integraion
#. Auto complete, snippets and static analysis
#. Minor tweaks
#. IDE-ization
#. Completely new paradigms


--------------------------------
Getting started with Vim Plugins
--------------------------------

**Get a package manager plugin first** (skip if you already have one)

For example `vim-plug <https://github.com/junegunn/vim-plug>`_:

#. Make sure ``git`` is installed.
#. Get ``vim-plug`` and install it into ``.vim/autoload``:

   * Vim: ``curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim``
   * NeoVim: ``curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim``
#. Open Vim/NeoVim config file:

   * Vim: ``vim ~/.vimrc``
   * NeoVim: ``nvim ~/.config/nvim/init.vim``

#. Add to the beginning of file:

   Vim::

      " specify the directory for plugins
      call plug#begin('~/.vim/plugged')

   NeoVim::

      " specify the directory for plugins
      call plug#begin('~/.local/share/nvim/plugged')

   Then add::

      " Example plugin: same as https://github.com/junegunn/vim-easy-align.git
      Plug 'junegunn/vim-easy-align'
      " initialize the plugin system
      call plug#end()

      " Map `ga` in normal mode to use EasyAlign
      nmap ga <Plug>(EasyAlign)
      " Map <Enter> in visual mode to use EasyAlign.
      vmap <Enter> <Plug>(EasyAlign)

#. Reload configuration by either running ``:so $MYVIMRC`` or restarting
   vim/nvim.

#. Run ``:PlugInstall``

You should now have a single plugin installed, ``vim-easy-align``.  Test it by
copying the text below to a file, and trying ``gaip=`` in normal mode (``ga``
- easy align, ``ip`` - in paragraph, ``=`` - align by ``=``.)

.. code::

   a = 1
   b   = 5
   c  = 3

To install any more plugins, add more ``Plug`` lines between ``plug#start``
and ``plug#end``


----------------------
Plugin recommendations
----------------------

- Do not install 10 plugins in a day.
- Do *not* install 100 plugins in a week.
- Take time to learn every plugin you try.

  If you can't learn a plugin, remove it.


.. TODO install more of these plugins, add sections for them
.. TODO find more/better 'language extension' plugins

..  " vim verbs for commenting
..  Plugin expand('tpope/vim-commentary')
..  " vim verbs for surrounding (in parens, quotes, tags...)
..  Plugin expand('tpope/vim-surround')
..  " vim verbs for alignment
..  Plugin expand('junegunn/vim-easy-align')
..  " '.' repeat support for vim-surround, vim-commentary and other plugins
..  Plugin expand('tpope/vim-repeat')
..  " Switch a piece of text between related options (e.g. true VS false, . VS -> etc.)
..  Plugin expand('andrewradev/switch.vim')
..  " Additional text objects such as delimiters ('*', ',', '+', ...) and seeking
..  " to next/last text object ('dinb', 'cil"' ...)
..  Plugin expand('wellle/targets.vim.git')
..  " Various pairwise utilities, such as for C string/XML/URL escaping, adding
..  " empty lines below/above current line, exchanging lines, toggling vim
..  " settings, jumping between VCS conflict markers...
..  Plugin expand('tpope/vim-unimpaired')
..  " f/t across lines, with target highlighting, and f/t replaces ; for search repeat
..  Plugin expand('rhysd/clever-f.vim')

------------------
Plugin showcase VM
------------------

TODO where to get VM, how to import into VirtualBox.


(Port, username, password)


Try whichever plugins seem interesting/useful.

------------
vim-surround
------------

``Plug 'tpope/vim-surround'``

Works with 'surroundings': parentheses, tags, quotes, etc.

Verbs:

* ``ds``: **delete surroundings**. ``dst`` deletes surrounding tags (``t``)
* ``cs``: **change surroundings**. ``cs"'`` changes surrounding double quotes to single quotes.
* ``ys``: **surround in**. ``ysw(`` surrounds word in parentheses.
* ``yss``: **surround current line** syntactic sugar. ``yssB`` surrounds the current line in curly brackets (Block).
* ``yS``: same as ``ys`` but places text on own line
* ``S``: **surround** in Visual mode; surrounds selection. e.g. ``viwS*`` selects and surrounds current word

Nouns:
* Specifics for ``ys`` and ``cs``:

  ``(`` surrounds with extra spaces: ``( surrounded ). ``)`` surrounds without spaces: ``(surrounded)``

  ``[``/``]``,``{``/``}`` work similarly.

  ``cs()`` will change ``( surrounded )`` to ``(surrounded)``

  ``s`` after ``ys``, or ``S`` after ``yS``, works on entire line. ``yssB`` will wrap the line in a Block ( curly brackets )


Custom surroundings can also be defined (useful for e.g. LaTeX) (see ``:help surround``)

.. TODO:

TODO example for defining custom surroundings

-----------------------
Exercise - vim-surround
-----------------------


What do these do? Try them:

#. dsB
#. ds*
#. dst
#. ysiw*
#. yssb
#. ySSB
#. cs,;
#. cst<strong>
#. ysiw[cs[]
#. V5jS{``
#. vi"S<div>``


Try your own combinations

--------------
vim-commentary
--------------

``Plug 'tpope/vim-commentary'``

Extends Vim language to handle comments (... unlike other comment handling plugins)

Comments depend on language support; this plugin does not do anything in plain
text files.

Verbs:
* ``gc``: comment. ``gcaB` comments out the current block.
* ``gcc``: comment current line. ``5gcc`` comments out 5 lines
* ``gcu``: uncomment current commented block.

Nouns:

* ``gc``: comment block. ``dgc`` deletes tthe current comment block


-------------------------
Exercise - vim-commentary
-------------------------

What do these do? Try them:


#. ``gc5j``
#. ``V5jgc``
#. ``8gcc``
#. ``gcu`` (inside a comment)
#. ``cgc``
#. ``>gc``


Try your own combinations

--------------
vim-easy-align
--------------

``Plug 'junegunn/vim-easy-align'``

Extends Vim language with text alignment features (useful for style, Python/YAML, tables) 

``vim-easy-align`` is disabled by default, must be manually mapped in your Vim
config. For example::

   " Map `ga` in normal mode to use EasyAlign
   nmap ga <Plug>(EasyAlign)
   " Map <Enter> in visual mode to use EasyAlign.
   vmap <Enter> <Plug>(EasyAlign)

**Normal mode syntax:**

EasyAlign syntax is a bit non-standard to Vim; the character to use for
alignment comes *after* the noun. For example, ``gaip=``:

#. ``ga``: Verb: *align* (as defined by the mapping above)
#. ``ip``: Noun: *inner paragraph*.
#. ``=``: "Alignment rule": says to *align around ``=``*

Try this on the following text::

    a=15
    string="aaa"
    var=434

**Visual mode syntax:**

The "alignment rule" goes after ``<Enter>`` (as mapped above).
For example ``vip<Enter>=``:

#. ``vip``: *select inner paragraph*
#. ``<Enter>``: Enter *alignment mode*
#. ``=``: *align around ``=``*


Alignment rules:

#. ``=``, ``.``, ``<Space>``, ``:``, ``,``, ``|``, ``&``, ``#``

**Aligment in multiple columns**:

#. ``*<Rule>``. For example, ``*=`` aligns using all ``=`` in each line

Try ``vip<Enter>*|`` on the following text::

   price|100|500|4554|5543
   weight|10|20|5|4
   power|10|5|43|45

**Left/Right/Center alignment**:

Repeating ``<Enter>`` in Visual mode changes alignment from Left (default) to
Right, then Center.

Try ``vip<Enter><Enter>*|`` on the above text.

**Live alignment**

Try ``vip<Enter><Ctrl-p>*|<Enter><Enter>``. Keep pressing ``<Enter>``, and finish
with ``<Ctrl-p>`` to apply the change. We can interactively see the
change we are doing. Also try ``vip<Enter><Ctrl-p>|<Enter><Enter><Enter>``.

This is cool although rather impractical; see if it suits you.

See ``:help easy-align`` for more info; there are many more features.

-------------------------
Exercise - vim-easy-align
-------------------------

What do these do? Try them:


#. ``gaip=``
#. ``viB<Enter>*<Space>``
#. ``ga5j,``

----------
vim-repeat
----------

``Plug 'tpope/vim-repeat'``

-----------
targets.vim
-----------

``Plug 'wellle/targets.vim.git'``


.. XXX some semi-graphical plugins, at least 2. See most popular at vim-awesome.

--------
See also
--------

* This workshop: https://gitlab.com/kiith/slides/tree/master/vim-workshop-2
* Vim in-depth: http://learnvimscriptthehardway.stevelosh.com
* Plugins: https://vimawesome.com
* .vimrc: http://dotshare.it/category/vim/rc/
* .vimrc: https://dotfiles.github.io/
* Plugins: https://www.reddit.com/r/vimplugins/
* Vim discussion: https://www.reddit.com/r/vim/
