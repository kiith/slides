.. =============================================
.. Intro do vyvoja na ARM (a ako si najst dosku)
.. =============================================
..
..
.. Vďaka nízkej cene a spotrebe energie architektúra ARM stala základom pre mnoho
.. experimentálnych projektov, a pomaly sa presadzuje aj v "mainstream" HPC
.. a serveroch. Na trhu je ale nepreberné množstvo lacných dosiek rôznej kvality
.. a ARM jadrá sa niekedy správajú inak než očakáva vývojár zvyknutý na x86 PC.
..
.. Táto prednáška pomôže vytvoriť si prehľad o tom ako nájsť ideálnu (alebo aspoň
.. použiteľnú) ARM dosku a tiež predstaví odlišnosti v správaní ARM jadier oproti
.. x86 čo sa týka výkonu.

.. XXX MERGING WITH OUR OSS WORKSOPS INITIATIVE?
.. xxx CHECK IF WE CAN CONNECT TO MONITOR


==================
Trains are awesome
==================

.. container:: fullscreen-image-v

   .. image:: train_1.jpg
     :align: center


--------------------------
mmcblk0... probably an ARM
--------------------------

.. container:: fullscreen-image-h

   .. image:: train_3.jpg
     :align: center

--------------------------
mmcblk0... probably an ARM
--------------------------

.. container:: fullscreen-image-h

   .. image:: train_4.png
     :align: center

-------
Contact
-------

Ferdinand Majerech

* v01d, ESET (don't emigrate yet!)

* https://gitlab.com/kiith
* https://github.com/kiith-sa
* https://v01d.sk
* ``kiithsacmp[AT]gmail.com``

Slides: https://gitlab.com/kiith/slides

=======================
The least bad ARM board
=======================

NOTE 1: This is all irrelevant if you use Windows. Don't use Windows.

NOTE 2: This is not a thoroughly researched talk. Pull requests welcome.

https://gitlab.com/kiith/cpp-benchmarks

-------------------------
TLDR: Get a Raspberry Pi.
-------------------------

* Unless you **really** need something specific
* Or you need to truly be free as in Stallman

--------------
Kernel support
--------------

Often *one* manufacturer-hacked kernel. Forget updates.

So you might get modern Debian... with kernel 3.14.

Good luck:

* Trying to hack a newer kernel to work
* Reconfiguring the kernel (why the **F** did they pick these flags?)
* Kernel-dependent tools/libs (perf, netfilter_queue ...)
* Security

*Make sure available kernel supports what you need*

Or **get a Raspberry Pi**: apparently supported in mainline kernel now

-----------
Development
-----------

* Don't expect copying a binary between boards to work

  (C library, ARMv8 vs v7 vs ..., etc.)

* Cross-compiling chain often needs to target specific board

* Or... **F** cross-compiling, compile on board

  With a 4-8 core board... why use a PC?


-------
Network
-------

* RPi and many boards: 100Mbit ethernet
* Can't find board with 2xGbE?  Find one with gigabit + USB3.

  Watch out for driver support... or slow USB3.

* WiFi?
* Multiple NICs? (e.g. router boards)

  Can the board **really** make use of them? (BPi R1)


-------------------------------
Connectivity - specs vs reality
-------------------------------

* Does USB3 really have USB3 speeds?
* What if **multiple** USB ports are used?
* What devices can it **power** without a hub?
* Does SATA/Ethernet/etc go through the USB bus?
* Is gigabit gigabit?

-------------
Upgradability
-------------

* SATA/M.2
* PCIe (x1/x4/x8/x16)?

  useful for more fast NICs; SSDs? even GPUs?

* **DIMM**/**SODIMM** slots ?
* Standard form-factor (Mini-ITX, RPi-compatible)?
* **SoM** + I/O board combinations

--------------
Other features
--------------

* GPU drivers?
* HW video decoding + drivers?
* GPIO?
* **Cooling** actually required? (RPi3, **XU4**)

etc.

-------------
ARM ecosystem
-------------

* ARM - CPU designs
* SoC vendors - reference boards, modified CPU designs
* Board vendors

  Yay! Get a custom board! $20/piece! Err. **$20k/1000pcs**
* "Users": Routers, STBs, Internet of Badly Secured Shit

Lots of competition, fast evolution, PITA standardization


========
ARM CPUs
========

---------------
Characteristics
---------------

* **Frequency/core count not so important**

* In-order/out of order? (instruction parallelism)
* big.LITTLE/heterogenous?
* cache size/parameters?
* ARMv7/v8/older?


-------------
On big.LITTLE
-------------


* Heterogenous CPU configs (e.g. 4 big A15 + 4 LITTLE A7)

  Or even 2+4+4

* Save power

.. image:: fire.jpg
   :width: 46%
   :align: right

* Linux may allow to use **all the coars**

  Not always a good idea

* Some tools expect homogenous CPUs

  May not work

  (``perf``)

* *LITTLE* much less powerful than *big*

  ``2.0Ghz != 2.0GHz``


---------------
Cortex-A series
---------------

High-end CPUs for '**Application**' use


* Most common 'high-perf' ARM CPU family

* Large differences between cores (think Pentium vs Pentium 3)

* Not today:

  **Cortex-R** - Real-time

  **Cortex-M** - Microcontrollers

-------------------------------
Cortex-A7(32b), Cortex-A53(64b)
-------------------------------

* LITTLE
* A-7: Raspberry Pi 2, old BananaPi boards, ODROID-XU4
* A-53: Raspberry Pi 3, Turris MOX, tons of random boards
* In-order CPU
* Low perf regardless of GHz

* Can't emulate Dreamcast
* 8-core A7 ... not worth much

--------------------------------------------
Cortex-A15/A17(32b), Cortex-A72/A73/A75(64b)
--------------------------------------------

* big
* High-end CPU
* Out-of-order
* A15/A17: ODROID-XU4, Dragonbox Pyra, Tinker Board
* A72: MacchiatoBin, RK3399(RockPro64, ODROID-N1, ...)

* **Can** emulate Dreamcast





-----------
Comparisons
-----------


* https://en.wikipedia.org/wiki/Comparison_of_ARMv8-A_cores
* https://en.wikipedia.org/wiki/Comparison_of_ARMv8-A_cores


============ =========
CPU          DMIPS/MHZ
============ =========
Cortex-A5    1.57
Cortex-A7    1.9
Cortex-A9    2.5
Cortex-A15   3.5-4
Cortex-A17   4
Cortex-A53   2.24
Cortex-A57   4.6
Cortex-A72   4.72
Cortex-A73   6.35
============ =========



---------------------
Future - architecture
---------------------

* Moore's **observation** no longer holds
* Heavier ARM cores
* Absurd core counts (Centriq, ThunderX)
* More heterogenous, specialized cores

  - GPU cores sharing memory (APU)
  - AI hardware

* Deeper memory hierarchies

  - HBM
  - X-Point and similar

-------------------
Upcoming - software
-------------------

* More standardization (please?)
* Better mainline kernel support (pretty please?)
* Vulkan and SPIR-V

  More open GPU programming, VR/AR

------------
Where to buy
------------

* ``rlx.sk``
* ``odroid.co.uk``
* ``aliexpress.com``
* ``rpishop.cz``

etc


====================
ARM core performance
====================

------------------
Performance vs x86
------------------

.. TODO cache matters! which data structure is better is counterintuitive
   - benchmark! (e.g. hashmap slower than binary search due to memory bloat)
   and benchmark this with varying data (because cache matters)

* Drivers/environment might be crap and slow you down

Slow:

* High-res timer access
* Allocations
* Synchronization
* Non-32bit ops (even on 64bit!)
* Virtual functions if they lead to cache misses
* Cache misses
* Divisions/modulo

**less forgiving to bad code (e.g. Java) than x86**

--------------
benchmarks-cpp
--------------

https://gitlab.com/kiith/cpp-benchmarks

Series of simple C++ benchmarks to get a 'feel' of a CPU

Arithmetic/Mutexes/Atomics/Allocations/etc

* Results are *best-case* - an operation runs in a loop

* Real-life performance **will** be worse


-----------------
Profiling is hard
-----------------

* ``perf`` may not be work with your snowflake kernel

  but a **huge** win if works - **worth a board just for this**
* ``callgrind`` completely broken on ARM
* ``gprof`` still useless
* manual timing/frame profiling... watch that timing overhead

.. TODO note about directly working with PMU after I read on that

---------------------
Performance resources
---------------------

* **Cortex-A Series Programmer's Guide for ARMv8-A** (copy here)

  https://static.docs.arm.com/den0024/a/DEN0024.pdf

* Jason D. Bakos: **Embedded Systems: ARM Programming and Optimization**

* Chris Simmonds: **Mastering Embedded Linux Programming**

* Agner Fog: **Optimizing Software in C++** (the first volume)

  http://www.agner.org/optimize/optimizing_cpp.pdf

  Written for x86, but most of this applies to ARM **even more**.

==================
Interesting boards
==================

------------------
(net) Banana PI R1
------------------

.. image:: bpi-r1.png
   :width: 80%
   :align: center

---------------------------
(net) SolidRun ClearFog Pro
---------------------------

.. image:: clearfog-pro.jpg
   :width: 80%
   :align: center

-------------------------
(net) Marvell ESPRESSObin
-------------------------

.. image:: espressobin.png
   :width: 70%
   :align: center

----------------------------------
(net-MiniITX) Marvell MACCHIATObin
----------------------------------

.. image:: macchiato.jpg
   :width: 85%
   :align: center

----------------------------------
(net-MiniITX) Marvell MACCHIATObin
----------------------------------

.. image:: macchiato-single.jpg
   :width: 85%
   :align: center



----------------
(net) Grapeboard
----------------

.. image:: grapeboard.png
   :width: 100%
   :align: center

----------
Odroid XU4
----------

.. image:: odroid-xu4.jpg
   :width: 100%
   :align: center

--------------
Tinker Board/S
--------------

.. image:: tinkerboard.png
   :width: 80%
   :align: center

--------------
Odroid HC1/HC2
--------------

.. image:: odroid-hc2.jpg
   :width: 47%
   :align: right

.. image:: odroid-hc1.jpg
   :width: 45%
   :align: left

----------
Odroid MC1
----------

.. image:: odroid-mc1.jpg
   :width: 55%
   :align: center

--------------
Firefly-RK3399
--------------

.. image:: firefly-rk3399.png
   :width: 70%
   :align: center

---------
RockPro64
---------

.. image:: rockpro64.jpg
   :width: 80%
   :align: center

---------
Odroid N1
---------

.. image:: odroid-n1.jpg
   :width: 90%
   :align: center

.. XXX macciato
.. XXX search CNX for more

-----
Links
-----


* http://www.cnx-software.com/

* https://www.board-db.org/


* https://www.reddit.com/r/linux_devices

* https://www.reddit.com/r/arm


----------------
V01D hackerspace
----------------

* Hackerspace in KE ( hackerspaces.org )
* Space for **DIY** HW/SW or just plain weird projects
* Currently doing: Bike hacking, game archeology, Neo/Vim, astrometry... soap?
* Meetups every other tuesday, next: ``Tue 2018-05-22 18:00``

* ``v01d.sk``, ``Národná Trieda 74`` (Snowden window), ``v01d-general-discussion@lists.v01d.sk``

**We will not build a mining rig for you**

----------
Random ads
----------

* ESET - don't emigrate yet
* OSSVikend KE - 20-21.10.2018

-------
The End
-------

(v01d)
